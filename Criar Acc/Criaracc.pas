unit Criaracc;

interface

uses PlayerData, MiscData,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TForm1 = class(TForm)
    Edit1: TEdit;
    Edit2: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Button1: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Edit2KeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
var
  f:file of TAccountFile;
  Acc: TAccountFile;
  local: string;
  i: Integer;
begin
  if (length(edit1.text) < 4) then
  begin
    Showmessage('Id curto demais. Deve ter pelo menos 4 digitos.');
    exit;
  end;
  if (length(edit2.Text) < 4) then
  begin
    Showmessage('Senha curta demais. Deve ter pelo menos 4 digitos.');
    exit;
  end;
  local:=getcurrentdir+'\'+edit1.Text[1]+'\'+edit1.Text+'.acc';
  if (FileExists(local)) then begin showmessage('Conta ja existente.'); exit; end;
  Zeromemory(@Acc,sizeof(TAccountFile));
  Acc.Header.UserName:=edit1.Text;
  Acc.Header.Password:=edit2.Text;
  for i := 0 to 3 do
    Acc.Characters[i].Base.Merchant := 255;
  AssignFile(f, local);
  ReWrite(f);
  Write(f,Acc);
  CloseFile(f);
end;

procedure TForm1.Edit2KeyPress(Sender: TObject; var Key: Char);
begin
if key = #13 then
button1.Click;
end;

end.
