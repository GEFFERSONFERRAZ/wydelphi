unit Load;

interface

uses Windows, SysUtils, Classes, MiscData, PlayerData, Dialogs, Functions, NPC, CombatHandlers;

type TLoad = class
  public
    class procedure InitCharacters(); static;
    class procedure ItemsList(); static;
    class procedure MobList(); static;
    class procedure QuestList(); static;
    class procedure InstantiateNPCs; static;
    class function InstantiateNPC(var npc: TNpc; var npcId: WORD; var errorsCount, errorsRead: Integer; const mobGenerData: TMOBGener; leaderId: Integer) : Boolean; static;
    class procedure TeleportList(); static;
    class function HeightMap: Boolean; static;
    class procedure SkillData(); static;
    class procedure MobBaby(); static;
end;

Const
  MAX_ITEMLIST = 6500;

implementation
{ TLoad }

uses GlobalDefs, Log, Util, Generics.Collections;

class function TLoad.HeightMap: Boolean;
var f: file of THeightMap;
  local: string;
begin
  ZeroMemory(@HeightGrid, SizeOf(THeightMap));
  local := 'HeightMap.dat';
  AssignFile(f, local);
  Reset(f);
  Read(f, HeightGrid);
  CloseFile(f);

  Logger.Write('HeightMap carregado com sucesso!', TLogType.ServerStatus);
  Result := true;
end;

class procedure TLoad.InitCharacters;
begin
  // TK
  ZeroMemory(@InitialCharacters[0], 4 * sizeof(TCharacter));
  InitialCharacters[0].ClassInfo:=0;InitialCharacters[0].Merchant:=1;InitialCharacters[0].CurrentScore.Level:=0;
  InitialCharacters[0].Equip[0].Index:=1;InitialCharacters[0].CurrentScore.Str:=8;InitialCharacters[0].CurrentScore.Int:=4;
  InitialCharacters[0].CurrentScore.Dex:=7;InitialCharacters[0].CurrentScore.Con:=6;InitialCharacters[0].pSkill:=0;
  InitialCharacters[0].CurrentScore.CurHP:=80; InitialCharacters[0].CurrentScore.MaxHP:=80; InitialCharacters[0].CurrentScore.Defense:=10;
  InitialCharacters[0].CurrentScore.CurMP:=45; InitialCharacters[0].CurrentScore.MaxMP:=45; InitialCharacters[0].CurrentScore.Attack:=10;
  InitialCharacters[0].BaseScore.CurHP:=80; InitialCharacters[0].BaseScore.MaxHP:=80; InitialCharacters[0].RegenHP:=8;
  InitialCharacters[0].BaseScore.CurMP:=45; InitialCharacters[0].BaseScore.MaxMP:=45; InitialCharacters[0].RegenMP:=8;
  InitialCharacters[0].Equip[1].Index:=1106; InitialCharacters[0].Equip[2].Index:=1118;
  InitialCharacters[0].Equip[3].Index:=1130; InitialCharacters[0].Equip[4].Index:=1142;
  InitialCharacters[0].Equip[5].Index:=1154; InitialCharacters[0].Gold:=1000;
  //InitialCharacters[0].pkstatus:=0;
  InitialCharacters[0].Equip[0].Effects[1].Index:=98; InitialCharacters[0].Equip[0].Effects[1].Value:=1;
  //FM
  InitialCharacters[1].ClassInfo:=1;InitialCharacters[1].Merchant:=11;InitialCharacters[1].CurrentScore.Level:=0;
  InitialCharacters[1].Equip[0].Index:=11;InitialCharacters[1].CurrentScore.Str:=5;InitialCharacters[1].CurrentScore.Int:=8;
  InitialCharacters[1].CurrentScore.Dex:=5;InitialCharacters[1].CurrentScore.Con:=5;InitialCharacters[1].pSkill:=0;
  InitialCharacters[1].CurrentScore.CurHP:=60; InitialCharacters[1].CurrentScore.MaxHP:=60; InitialCharacters[1].CurrentScore.Defense:=10;
  InitialCharacters[1].CurrentScore.CurMP:=65; InitialCharacters[1].CurrentScore.MaxMP:=65; InitialCharacters[1].CurrentScore.Attack:=10;
  InitialCharacters[1].BaseScore.CurHP:=60; InitialCharacters[1].BaseScore.MaxHP:=60; InitialCharacters[1].RegenHP:=8;
  InitialCharacters[1].BaseScore.CurMP:=65; InitialCharacters[1].BaseScore.MaxMP:=65; InitialCharacters[1].RegenMP:=8;
  InitialCharacters[1].Equip[1].Index:=1256; InitialCharacters[1].Equip[2].Index:=1268;
  InitialCharacters[1].Equip[3].Index:=1280; InitialCharacters[1].Equip[4].Index:=1292;
  InitialCharacters[1].Equip[5].Index:=1304; InitialCharacters[1].Gold:=1000;
  //InitialCharacters[1].pkstatus:=0;
  InitialCharacters[1].Equip[0].Effects[1].Index:=98; InitialCharacters[0].Equip[0].Effects[1].Value:=1;
  //BM
  InitialCharacters[2].ClassInfo:=2;InitialCharacters[2].Merchant:=21;InitialCharacters[2].CurrentScore.Level:=0;
  InitialCharacters[2].Equip[0].Index:=21;InitialCharacters[2].CurrentScore.Str:=6;InitialCharacters[2].CurrentScore.Int:=6;
  InitialCharacters[2].CurrentScore.Dex:=9;InitialCharacters[2].CurrentScore.Con:=5;InitialCharacters[2].pSkill:=0;
  InitialCharacters[2].CurrentScore.CurHP:=70; InitialCharacters[2].CurrentScore.MaxHP:=70; InitialCharacters[2].CurrentScore.Defense:=10;
  InitialCharacters[2].CurrentScore.CurMP:=55; InitialCharacters[2].CurrentScore.MaxMP:=55; InitialCharacters[2].CurrentScore.Attack:=10;
  InitialCharacters[2].BaseScore.CurHP:=70; InitialCharacters[2].BaseScore.MaxHP:=70; InitialCharacters[2].RegenHP:=8;
  InitialCharacters[2].BaseScore.CurMP:=55; InitialCharacters[2].BaseScore.MaxMP:=55; InitialCharacters[2].RegenMP:=8;
  InitialCharacters[2].Equip[1].Index:=1418; InitialCharacters[2].Equip[2].Index:=1421;
  InitialCharacters[2].Equip[3].Index:=1424; InitialCharacters[2].Equip[4].Index:=1427;
  InitialCharacters[2].Equip[5].Index:=1430; InitialCharacters[2].Gold:=1000;
  //InitialCharacters[2].pkstatus:=0;
  InitialCharacters[2].Equip[0].Effects[1].Index:=98; InitialCharacters[0].Equip[0].Effects[1].Value:=1;
  //HT
  InitialCharacters[3].ClassInfo:=3;InitialCharacters[3].Merchant:=31;InitialCharacters[3].CurrentScore.Level:=0;InitialCharacters[3].Equip[0].Index:=31;
  InitialCharacters[3].CurrentScore.Str:=8;InitialCharacters[3].CurrentScore.Int:=9;InitialCharacters[3].CurrentScore.Dex:=13;
  InitialCharacters[3].CurrentScore.Con:=6;InitialCharacters[3].pSkill:=0;
  InitialCharacters[3].CurrentScore.CurHP:=75; InitialCharacters[3].CurrentScore.MaxHP:=75; InitialCharacters[3].CurrentScore.Defense:=10;
  InitialCharacters[3].CurrentScore.CurMP:=60; InitialCharacters[3].CurrentScore.MaxMP:=60; InitialCharacters[3].CurrentScore.Attack:=10;
  InitialCharacters[3].BaseScore.CurHP:=75; InitialCharacters[3].BaseScore.MaxHP:=75; InitialCharacters[3].RegenHP:=8;
  InitialCharacters[3].BaseScore.CurMP:=3000; InitialCharacters[3].BaseScore.MaxMP:=3000; InitialCharacters[3].RegenMP:=8;
  InitialCharacters[3].Equip[1].Index:=1567; InitialCharacters[3].Equip[2].Index:=1571;
  InitialCharacters[3].Equip[3].Index:=1574; InitialCharacters[3].Equip[4].Index:=1577;
  InitialCharacters[3].Equip[5].Index:=1580; InitialCharacters[3].Gold:=1000;
  //InitialCharacters[3].pkstatus:=0;
  InitialCharacters[3].Equip[0].Effects[1].Index:=98; InitialCharacters[0].Equip[0].Effects[1].Value:=1;

{
  charNames := ['TK', 'FM', 'BM', 'HT'];
  for charName in charNames do
  begin
    local := Diretorio + '\InitChars\' + charName + '.npc';
    if(FileExists(local) = false) then begin
      Showmessage('InitChar: ' + charName + ' n�o encontrado.');
      continue;
    end;
    AssignFile(fs, local);
    Reset(fs);
    Read(fs, InitCharacters[i]);
    CloseFile(fs);
    InitialCharacters[i].Status := InitialCharacters[i].BaseScore
    Inc(i);
  end;
  Logger.Write('Personagens iniciais carregados com sucesso', TLogType.ServerStatus);
}
end;

class procedure TLoad.ItemsList;
var f: TFileStream;
buffer: array of BYTE;
local: string;
I,j, size: Integer;
ItemListB: array of TItemList;
Item : TItemList;
begin
  local:=getcurrentdir+'\ItemList.bin';

  if not(FileExists(local)) then
  begin
    showmessage('Arquivo n�o encontrado. Coloque o ItemList.bin na mesma pasta que o Leitor de ItemList.');
    exit;
  end;

  F := TFileStream.Create(local, fmOpenRead);
  size := F.Size;
  SetLength(buffer,size);
  F.Read(buffer[0],size);
  F.Free;

  for I := 0 to size do
  begin
    buffer[i] := $5A xor buffer[i];
  end;

  setlength(ItemListB, Round(size/140));
  Move(buffer[0], ItemListB[0], size);

  j := Round(size/140);
  for I := 0 to j do
  begin
    //ZeroMemory(@Item,sizeof(TItemList));
    //Move(ItemListB[i].Name[0],Item.Name,sizeof(TItemListConvert));
    //Item.Index := i;
    ItemList.Add(i, ItemListB[i]);
  end;

  Logger.Write('ItemList carregado com sucesso!', TLogType.ServerStatus);
end;

class procedure TLoad.MobBaby;
  var i: BYTE;
      local : string;
      fs: File of TCharacter;
      erro: boolean;
      mob : TCharacter;
      errorCount: integer;
begin
  MobBabyList.Clear;
  erro := false;
  errorCount := 0;
  for i := 0 to (MAX_MOB_BABY - 1) do
  begin
    try
      if(MobBabyNames[i] = '')then
          continue;

      local := CurrentDir + '\npc_base\' + MobBabyNames[i];
      if(FileExists(local) = false) then
      begin
        Logger.Write('NpcBase: ' + MobBabyNames[i] + ' n�o encontrado.', TLogType.Warnings);
        //Showmessage('Npc: ' + MobBabyNames[i] + ' n�o encontrado.');
        continue;
        erro := true;
      end;
      AssignFile(fs, local);
      Reset(fs);
      Read(fs, mob);
      CloseFile(fs);
      mob.CurrentScore := mob.BaseScore;
      MobBabyList.Add(mob);
    except
      erro := true;
      inc(errorCount);
      Logger.Write('NpcBase: ' + MobBabyNames[i] + ' com erro de leitura.', TLogType.Warnings);
      CloseFile(fs);
    end;
  end;
  if(erro)then
    Logger.Write('MountBaby carregado com' + inttostr(errorCount) + 'erros!', TLogType.ServerStatus)
  else Logger.Write('MountBaby carregado com sucesso!', TLogType.ServerStatus);
end;


class function TLoad.InstantiateNPC(var npc: TNpc; var npcId: WORD; var errorsCount, errorsRead: Integer; const mobGenerData: TMOBGener; leaderId: Integer) : Boolean;
var npcName: string;
  npcDirectory, followerDir: string;
  storeCount, nextSlot: BYTE;
  npcFile: file of TCharacter;
  npcCharacter: TCharacter;
  i: BYTE;
begin
  Result := false;

  try
    npcName := IfThen(leaderId = npcId, mobGenerData.Leader, mobGenerData.Follower);
    npcDirectory := CurrentDir + '\NPC\' + npcName;
    if(FileExists(npcDirectory) = false) then
    begin
      Logger.Write('Npc: ' + npcName + ' n�o encontrado.', TLogType.Warnings);
      Inc(errorsCount);
      exit;
    end;


    AssignFile(npcFile, npcDirectory);
    Reset(npcFile);
    Read(npcFile, npcCharacter);
    CloseFile(npcFile);


    npc.Create(npcCharacter, mobGenerData.Id, npcId, leaderId);
    npc.Character.Last := mobGenerData.SpawnSegment.Position;
    if(npc.Character.Merchant = 1) then
    begin
      storeCount := 0;
      for i := 0 to 64 do
      begin
        if(storeCount >= 26) then break;
        if(npc.Character.Inventory[i].Index = 0) then
        begin
          for nextSlot := i + 1 to 64 do
          begin
            if(npc.Character.Inventory[nextSlot].Index <> 0) then
            begin
              Move(npc.Character.Inventory[nextSlot], npc.Character.Inventory[i], SizeOf(TItem));
              ZeroMemory(@npc.Character.Inventory[nextSlot], SizeOf(TItem));
              Inc(storeCount);
              break;
            end;
          end;
        end
        else Inc(storeCount);
      end;
    end
    else if(npc.Character.Merchant = 19) then
    begin
      storeCount := 0;
      for i := 0 to 64 do
      begin
        if(npc.Character.Inventory[i].Index <> 0) and (storeCount <= 26) then
        begin
          Move(npc.Character.Inventory[i], npc.Character.Inventory[storeCount], SizeOf(TItem));
          inc(storeCount);
          if(storeCount = 8) or (storeCount = 17) then
            inc(storeCount);
        end;
      end;
    end;

    if (mobGenerData.MinuteGenerate = 0) then
    begin
      if not(TFunctions.GetEmptyMobGrid(npcId, npc.Character.Last, 8)) then
      begin
        Result := true;
        exit;
      end;
      MobGrid[npc.Character.Last.Y][npc.Character.Last.X] := npc.base.ClientId;
    end
    else
    begin
      NPC.Character.CurrentScore.CurHP := 0;
      npc.TimeKill := Server.UpTime;
    end;

    if(npc.Character.BaseScore.MoveSpeed > 10) then
      npc.Character.BaseScore.MoveSpeed := 2
    else if(npc.Character.BaseScore.MoveSpeed = 0) then
      npc.Character.BaseScore.MoveSpeed := 1;
    npc.Character.CurrentScore.MoveSpeed := npc.Character.BaseScore.MoveSpeed;

//    NPCs[npc.Base.ClientId] := npc;
//    NPCs[npc.Base.ClientId].UpdateCharacterPointer();
    Inc(npcId);
  except
    Logger.Write('Npc: ' + npcName + ' com erro de leitura.', TLogType.Warnings);
    Inc(errorsRead);
    CloseFile(npcFile);
    Result := true;
  end;
  Result := true;
end;

class procedure TLoad.InstantiateNPCs;
var errorsCount, errorsRead, i, generId : Integer;
    mobGenerData: TMOBGener;

    leaderCharacterFile, followerCharacterFile: file of TCharacter;
    npcId, followerId, leaderId: WORD;
//    npc, follower : TNpc;
    npcsErrors: string;
begin
  npcId := 1001;
  errorsCount := 0;
  errorsRead  := 0;
  ZeroMemory(@NPCs, SizeOf(NPCs));

  for generId := 0 to (MobGener.Count - 1) do
  begin
    try
      mobGenerData := MobGener[generId];
      if not(InstantiateNpc(NPCs[npcId], npcId, errorsCount, errorsRead, mobGenerData, npcId)) then
      begin
        continue;
      end;

      if(mobGenerData.MinGroup < 1) AND (mobGenerData.MaxGroup < 1) then
        continue;

      leaderId := npcId - 1;
      for followerId := mobGenerData.MinGroup to mobGenerData.MaxGroup do
      begin
        InstantiateNPC(NPCs[npcId], npcId, errorsCount, errorsRead, mobGenerData, leaderId);
      end;
    except
      Inc(errorsRead);
    end;
  end;
  InstantiatedNPCs := npcId;

  if(errorsCount > 0) then
    Logger.Write(IntToStr(errorsCount)+ ' mobs n�o encontrados!', TLogType.ServerStatus);

  if(errorsRead > 0) then
    Logger.Write(IntToStr(errorsRead) + ' mobs com erro de leitura!', TLogType.ServerStatus);
end;

class procedure TLoad.MobList;
var DataFile : TextFile;
  line : string;
  aux: TMOBGener;
  i : BYTE;
begin
  MobGener.Clear;

  AssignFile(DataFile, 'NPCGener.txt');
  Reset(DataFile);

  ZeroMemory(@aux, sizeof(TMOBGener));
  aux.ID := -1;
  while not EOF(DataFile) do
  begin
    readln(DataFile, line);

    if (Trim(line) = '') then
      continue;

    if Pos('#',line) > 0 then
    begin
      Delete(line, Pos('#',line), 1);
      Delete(line, Pos('[',line), 1);
      Delete(line, Pos(']',line), 1);
      aux.ID := StrToIntDef(Trim(line),0);
      continue;
    end
    else if Pos('MinuteGenerate',line) > 0 then
    begin
      Delete(line,Pos('MinuteGenerate',line),Length('MinuteGenerate'));
      Delete(line,Pos(':',line),1);
      aux.MinuteGenerate := StrToIntDef(Trim(line),0);
    end
    else if Pos('MaxNumMob',line) > 0 then
    begin
      Delete(line,Pos('MaxNumMob',line),Length('MaxNumMob'));
      Delete(line,Pos(':',line),1);
      aux.MaxNumMob := StrToIntDef(Trim(line),0);
    end
    else if Pos('MinGroup',line) > 0 then
    begin
      Delete(line,Pos('MinGroup',line),Length('MinGroup'));
      Delete(line,Pos(':',line),1);
      aux.MinGroup := StrToIntDef(Trim(line),0);
    end
    else if Pos('MaxGroup',line) > 0 then
    begin
      Delete(line,Pos('MaxGroup',line),Length('MaxGroup'));
      Delete(line,Pos(':',line),1);
      aux.MaxGroup := StrToIntDef(Trim(line),0);
    end
    else if Pos('Leader',line) > 0 then
    begin
      Delete(line,Pos('Leader',line), Length('Leader'));
      Delete(line,Pos(':',line),1);
      aux.Leader := Trim(line);
    end
    else if Pos('Follower',line) > 0 then
    begin
      Delete(line,Pos('Follower',line),Length('Follower'));
      Delete(line,Pos(':',line),1);
      aux.Follower := Trim(line);
    end
    else if Pos('RouteType',line) > 0 then
    begin
      Delete(line,Pos('RouteType',line),Length('RouteType'));
      Delete(line,Pos(':',line),1);
      aux.RouteType := StrToIntDef(Trim(line),0);
    end
    else if Pos('Formation',line) > 0 then
    begin
      Delete(line,Pos('Formation',line),Length('Formation'));
      Delete(line,Pos(':',line),1);
      aux.Formation := StrToIntDef(Trim(line),0);
    end
    else if Pos('StartX',line) > 0 then
    begin
      Delete(line,Pos('StartX', line),Length('StartX'));
      Delete(line,Pos(':',line),1);
      aux.SpawnSegment.Position.X := StrToIntDef(Trim(line),0);
    end
    else if Pos('StartY',line) > 0 then
    begin
      Delete(line,Pos('StartY',line),Length('StartY'));
      Delete(line,Pos(':',line),1);
      aux.SpawnSegment.Position.Y := StrToIntDef(Trim(line),0);
    end
    else if Pos('StartWait',line) > 0 then
    begin
      Delete(line,Pos('StartWait',line),Length('StartWait'));
      Delete(line,Pos(':',line), 1);
      aux.SpawnSegment.WaitTime := StrToIntDef(Trim(line),0);
    end
    else if (Pos('Segment',line) > 0) and (Pos('X',line) > 0) then
    begin
      Delete(line,Pos('Segment',line),Length('Segment'));
      i := StrToIntDef(Copy(Trim(line),1,1), 0);
      if i = 0 then
        continue;
      Delete(line,Pos(inttostr(i),line),1);
      Delete(line,Pos('X',line),1);
      Delete(line,Pos(':',line),1);

      aux.AddSegmentData(i, StrToIntDef(Trim(line), 0), -1);
    end
    else if (Pos('Segment',line) > 0) and (Pos('Y', line) > 0) then
    begin
      Delete(line,Pos('Segment',line), Length('Segment'));
      i := StrToIntDef(Copy(Trim(line),1,1), 0);
      if i = 0 then
        continue;
      Delete(line,Pos(inttostr(i),line),1);
      Delete(line,Pos('Y',line),1);
      Delete(line,Pos(':',line),1);
      aux.AddSegmentData(i, -1, StrToIntDef(Trim(line), 0));
    end
    else if (Pos('Segment',line) > 0) and (Pos('Wait', line) > 0) then
    begin
      Delete(line,Pos('Segment',line), Length('Segment'));
      i := StrToIntDef(Copy(Trim(line),1,1), 0);
      if i = 0 then
        continue;
      Delete(line,Pos(inttostr(i),line),1);
      Delete(line,Pos('Wait',line),1);
      Delete(line,Pos(':',line),1);
      aux.AddSegmentData(i, StrToIntDef(Trim(line), 0));
    end
    else if (Pos('Segment',line) > 0) and (Pos('Action', line) > 0) then
    begin
      Delete(line,Pos('Segment',line), Length('Segment'));
      i := StrToIntDef(Copy(Trim(line),1,1), 0);
      if i = 0 then
        continue;
      Delete(line, Pos(inttostr(i),line),1);
      Delete(line, Pos('Action', line), Length('Action'));
      Delete(line, Pos(':',line), 1);
      aux.AddSegmentData(i, Trim(line));
    end
    else if Pos('********************', line) > 0 then
    begin
      if aux.ID = -1 then
        continue;
      MobGener.Add(aux.ID, aux);
      ZeroMemory(@aux, sizeof(TMOBGener));
    end;
  end;
  CloseFile(DataFile);

  TLoad.InstantiateNPCs;
  Logger.Write(IntToStr(InstantiatedNPCs) + ' mobs foram instanciados!', TLogType.ServerStatus)
end;

class procedure TLoad.QuestList;
var DataFile : TextFile;
    lineFile : String;
    fileStrings : TStringList;
    quest: TQuest;
    capacity: Int8;
begin
  ZeroMemory(@quest, sizeof(TQuest));
  Quests.Clear;

  AssignFile(DataFile, 'QuestList.txt');
  Reset(DataFile);

  fileStrings := TStringList.Create;

  while not EOF(DataFile) do
  begin
    Readln(DataFile, lineFile);
    ExtractStrings([','],[' '],PChar(Linefile),fileStrings);
    if(TFunctions.IsNumeric(fileStrings.strings[0], quest.Index) = false) then begin
      filestrings.Clear;
      continue;
    end;

    quest.ItemId := StrToInt(fileStrings.strings[1]);
    quest.ResetTime := StrToInt(fileStrings.strings[2]);
    quest.QuestPosition.X := StrToInt(fileStrings.strings[3]);
    quest.QuestPosition.Y := StrToInt(fileStrings.strings[4]);
    quest.QuestArea.BottomLeft.X := StrToInt(fileStrings.strings[5]);
    quest.QuestArea.BottomLeft.Y := StrToInt(fileStrings.strings[6]);
    quest.QuestArea.TopRigth.X := StrToInt(fileStrings.strings[7]);
    quest.QuestArea.TopRigth.Y := StrToInt(fileStrings.strings[8]);

    quest.Players.Create;
    quest.Players.Capacity := StrToInt(fileStrings.strings[9]);

    filestrings.Clear;
    Quests.Add(quest)
  end;
  fileStrings.Free;
  Logger.Write('QuestList carregado com sucesso!', TLogType.ServerStatus);
  CloseFile(DataFile);
end;

class procedure TLoad.SkillData;
var DataFile : TextFile;
    lineFile : String;
    fileStrings : TStringList;
    skill : TSkillData;
    skillId : Integer;
begin
  SkillsData.Clear;

  AssignFile(DataFile, 'Skilldata.csv');
  Reset(DataFile);

  fileStrings := TStringList.Create;

  while not EOF(DataFile) do
  begin
    Readln(DataFile, lineFile);
    ExtractStrings([','],[' '],PChar(Linefile),fileStrings);
    if(TFunctions.IsNumeric(fileStrings.strings[0], skillId) = false)then begin
      filestrings.Clear;
      continue;
    end;
    skill.Index         := skillId;
    skill.SkillPoint    := StrToIntDef(fileStrings.Strings[1],0);
    skill.TargetType    := StrToIntDef(fileStrings.Strings[2],-1);
    skill.Name          := fileStrings.Strings[22];
    skill.ManaSpent     := StrToIntDef(fileStrings.Strings[3], 0);
    skill.Delay         := StrToIntDef(fileStrings.Strings[4], 0);

    skill.InstanceType  := fileStrings.Strings[6];
    skill.InstanceValue := fileStrings.Strings[7];
    skill.AffectValue   := fileStrings.Strings[11];
    skill.AffectTime    := StrToIntDef(fileStrings.Strings[12],0);

    filestrings.Clear;
    SkillsData.Add(skill)
  end;
  fileStrings.Free;
  Logger.Write('SkillData carregado com sucesso!', TLogType.ServerStatus);
  CloseFile(DataFile);

  TCombatHandlers.RegisterSkills;
end;


class procedure TLoad.TeleportList;
var DataFile : TextFile;
    lineFile : String;
    fileStrings : TStringList;
    teleport : TTeleport;
begin
  TeleportsList.Clear;

  AssignFile(DataFile, 'Teleports.csv');
  Reset(DataFile);

  fileStrings := TStringList.Create;

  while not EOF (DataFile) do
  begin
    Readln(DataFile, lineFile);
    ExtractStrings([','], [' '], pChar(Linefile), fileStrings);

    if(TFunctions.IsNumeric(fileStrings.strings[0]) = false)then begin
      filestrings.Clear;
      continue;
    end;

    //Adiciona na estrutura;
    teleport.Scr1.X := strtoint(fileStrings.Strings[0]);
    teleport.Scr1.Y := strtoint(fileStrings.Strings[1]);//nome
    teleport.Dest1.X := strtoint(fileStrings.Strings[2]);//
    teleport.Dest1.Y := strtoint(fileStrings.Strings[3]);//
    teleport.Price := strtoint(fileStrings.Strings[4]);//
    teleport.Time := strtoint(fileStrings.Strings[5]);//
    filestrings.Clear;
    TeleportsList.Add(teleport);
  end;
  fileStrings.Free;
  Logger.Write('Teleportes carregados com sucesso!', TLogType.ServerStatus);
  CloseFile(DataFile);
end;

end.
