unit PacketHandlers;

interface

uses Windows, ClientConnection, Packets, Player, Functions, PlayerData, SysUtils, MiscData,
    ItemFunctions, NPC;


type TPacketHandlers = class(TObject)
  public
    class function CheckLogin(var player : TPlayer; var buffer: array of Byte) : Boolean; static;
    class function NumericToken(var player : TPlayer; var buffer: array of Byte) : Boolean; static;
    class function CreateCharacter(var player: TPlayer; var buffer: array of Byte) : Boolean; static;
    class function DeleteCharacter(var player: TPlayer; var buffer: array of Byte) : Boolean; static;
    class function SelectCharacter(var player: TPlayer; var buffer: array of Byte) : Boolean; static;
    class function PlayerChat(var player: TPlayer; var buffer: array of Byte): Boolean; static;
    class function Gates(var player : TPlayer; var buffer: array of Byte) : Boolean; static;
    class function AddPoints(var player: TPlayer; var buffer: array of Byte): boolean;
    class function ChangeCity(var player : TPlayer; var buffer: array of Byte) : Boolean; static;
    class function MoveItem(var player : TPlayer; var buffer: array of Byte) : Boolean; static;
    class function DeleteItem(var player: TPlayer; var buffer: array of Byte) : Boolean;
    class function UngroupItem(var player: TPlayer; var buffer: array of Byte) : Boolean;
    class function SendNPCSellItens(var player: TPlayer; var buffer: array of Byte) : Boolean; static;
    class function BuyNpcItens(var player: TPlayer; var buffer: array of Byte): Boolean; static;
    class function SellItemsToNPC(var player: TPlayer; var buffer: array of Byte) : Boolean; static;
    class function RequestOpenStoreTrade(var player: TPlayer; var buffer: array of Byte): Boolean; static;
    class function BuyStoreTrade(var player : TPlayer; var buffer: array of Byte) : Boolean; static;
    class function RequestParty(var player: TPlayer; var buffer: array of Byte): Boolean; static;
    class function ExitParty(var player: TPlayer; var buffer: array of Byte): Boolean; static;
    class function AcceptParty(var player: TPlayer; var buffer: array of Byte): Boolean; static;

    class function SendClientSay(var player: TPlayer; var buffer: array of Byte): Boolean;
    class function CargoGoldToInventory(var player: TPlayer; var buffer: array of Byte): Boolean;
    class function InventoryGoldToCargo(var player: TPlayer; var buffer: array of Byte): Boolean;



    class function Trade(var player: TPlayer; var buffer: array of Byte): Boolean;

    class function CloseTrade(var player: TPlayer): Boolean;
    class function OpenStoreTrade(var player: TPlayer; var buffer: array of Byte): boolean;
    class function PKMode(var player: TPlayer; var buffer: array of Byte): boolean;
    class function ChangeSkillBar(var player: TPlayer; var buffer: array of Byte): boolean;
    class function DropItem(var player: TPlayer; var buffer: array of Byte): boolean;
    class function PickItem(var player: TPlayer; var buffer: array of Byte): boolean;



    class function MovementCommand(var player: TPlayer; var buffer: array of Byte): Boolean;

    class function RequestEmotion(var player: TPlayer; var buffer: array of Byte): Boolean;

end;

implementation
uses GlobalDefs, Log, Util;

class function TPacketHandlers.ChangeCity(var player: TPlayer; var buffer: array of Byte): Boolean;
begin
  Move(buffer[12], player.Character.CurrentCity, 4);
  if(player.Character.CurrentCity < TCity.Armia) or (player.Character.CurrentCity > TCity.Karden) then
  begin
    player.Character.CurrentCity := TCity.Armia;
    exit;
  end;
  Result := true;
end;

class function TPacketHandlers.RequestEmotion(var player: TPlayer; var buffer: array of Byte): Boolean;
var packet : TRequestEmotion absolute buffer;
begin
  Result := false;
  player.SendEmotion(packet.effType, packet.effValue);
  Result := true;
end;

class function TPacketHandlers.CheckLogin(var player : TPlayer; var buffer : array of Byte) : Boolean;
var packet : TRequestLoginPacket absolute buffer;
    userName, passWord : string;
begin
  Result := false;
  if(packet.Version <> 755) then
  begin
    player.SendClientMessage('Atualize o client!');
    exit;
  end;

  userName := TFunctions.CharArrayToString(packet.UserName);

  if not(player.LoadAccount(userName)) then
  begin
     player.SendClientMessage('Conta n�o encontrada!');
     exit;
  end;

  passWord := TFunctions.CharArrayToString(packet.PassWord);

  if(player.Account.Header.Password <> passWord) then
  begin
    player.SendClientMessage('Senha incorreta!');
    Server.Disconnect(player);
    exit;
  end;

  if(player.Account.Header.IsActive) then
  begin
    player.SendClientMessage('Conex�o anterior finalizada!');
    Server.Disconnect(player.Account.Header.Username);
    exit;
  end;
  player.Account.Header.IsActive := true;
  player.SaveAccount;
  player.SendCharList;
  Result := true;
end;

class function TPacketHandlers.NumericToken(var player: TPlayer; var buffer: array of Byte): Boolean;
var packet : TNumericTokenPacket absolute buffer;
begin
  Result := true;
  if((player.Account.Header.NumericToken = '') OR (packet.RequestChange = 1)) then
  begin
    player.Account.Header.NumericToken := packet.Num;
    player.SaveAccount;
    player.SubStatus := Waiting;
    //exit;
  end
  else
  begin
    if (AnsiCompareText(packet.num, player.Account.Header.NumericToken) = 0) then
    begin
      player.SubStatus := WAITING;
    end
    else
    begin
      packet.Header.Code := $FDF;
    end;
    packet.Header.Size := 12;
    packet.Header.Index := 30002;
    player.SendPacket(@packet, 12);
  end;
end;

class function TPacketHandlers.RequestOpenStoreTrade(var player: TPlayer; var buffer: array of Byte): Boolean;
var packet: TRequestOpenPlayerStorePacket absolute buffer; i,z: BYTE;
begin
  if(player.Character.IsStoreOpened = true)then
  begin
    result := true;
    exit;
  end;

  for i := 0 to 11 do
  begin
    if(packet.Trade.Gold[i] = 0)then
      continue;

    if(packet.Trade.Slot[i] >= 128)then
    begin
      result := false;
      exit;
    end;

    z := 0;
    while z < 12 do
    begin
      if(z <> i) and (packet.Trade.Slot[i] = packet.Trade.Slot[z]) then
        break;
      inc(z);
    end;

    if(z <> 12) then
    begin
      result := false;
      exit;
    end;

    if(packet.Trade.Gold[i] > 999999999)then
    begin
      result:=false;
      exit;
    end;

    if(player.Account.Header.StorageItens[packet.Trade.Slot[i]].Index > 6500) then
    begin
      result := false;
      exit;
    end;

    if(CompareMem(@player.Account.Header.StorageItens[packet.Trade.Slot[i]], @packet.Trade.Item[i], 8) = false) then
    begin
      result := false;
      exit;
    end;
  end;

  player.Character.IsStoreOpened := true;
  Move(packet.Trade, player.Character.TradeStore, sizeof(TTradeStore));
  player.SendPacket(@packet, packet.Header.Size);
  player.Base.SendCreateMob;
  result:=true;
end;

class function TPacketHandlers.CreateCharacter(var player: TPlayer; var buffer: array of Byte): Boolean;
var packet : TCreateCharacterRequestPacket absolute buffer;
begin
  Result := true;
  if (packet.ClassIndex < 0) or (packet.ClassIndex > 3) then begin
    player.SendClientMessage('Classe Inv�lida.');
    exit;
  end;
  if (packet.SlotIndex < 0) or (packet.SlotIndex > 3) then begin
    player.SendClientMessage('Posi��o Inv�lida.');
    exit;
  end;
  if (player.Account.Characters[packet.SlotIndex].Base.Equip[0].Index <> 0) then begin
    player.SendClientMessage('Voc� j� possui personagem nesse slot.');
    exit;
  end;
  if (TFunctions.FindCharacter(packet.Name)) then begin
    player.SendClientMessage('Este nome j� est� sendo utilizado.');
    exit;
  end;


  Move(InitialCharacters[packet.ClassIndex], player.Account.Characters[packet.SlotIndex].Base, sizeof(TCharacter));
  //player.Account.Characters[packet.SlotIndex].PlayerKill := 0;

  Move(packet.Name, player.Account.Characters[packet.SlotIndex].Base.Name[0], 15);

//  player.Account.Characters[packet.SlotIndex].Base.Merchant:=0;
  player.Account.Characters[packet.SlotIndex].Base.Hold:=0;
  player.Account.Characters[packet.SlotIndex].Base.CurrentScore.MoveSpeed := 2;
  player.Account.Characters[packet.SlotIndex].Base.Last := TFunctions.GetStartXY(player, packet.SlotIndex);

  player.SendCharList($110);

  //Logger.Write('O jogador [' + player.Account.Header.Username+'] Criou personagem: [' + packet.Name + '].', TLogType.ServerStatus);

  TFunctions.SaveCharacterFile(player, packet.Name);
  player.SaveAccount;
end;


class function TPacketHandlers.DeleteCharacter(var player: TPlayer; var buffer: array of Byte): Boolean;
var packet : TDeleteCharacterRequestPacket absolute buffer;
    local : string;
begin
  Result := true;
  if (packet.Password <> player.Account.Header.Password) then begin
    player.SendClientMessage('Senha incorreta.');
    exit;
  end;

  if (player.Account.Characters[packet.SlotIndex].Base.Equip[0].Index = 0) then begin
    player.SendClientMessage('Voc� n�o possui personagem neste slot.');
    exit;
  end;

  if not(TFunctions.FindCharacter(packet.Name)) then begin
    player.SendClientMessage('Este personagem n�o existe.');
    exit;
  end;

  if not(TFunctions.CompareCharOwner(player, packet.Name)) then
  begin
    player.SendClientMessage('Este personagem n�o pertence a esta conta.');
    exit;
  end;

  if(TFunctions.IsLetter(packet.Name)) then
    local := CurrentDir + '\Chars\' + packet.Name[0] + '\'+ Trim(packet.Name)
  else
    local := CurrentDir + '\Chars\etc\'+ Trim(packet.Name);
  DeleteFile(local);

  ZeroMemory(@player.Account.Characters[packet.SlotIndex], sizeof(TCharacterDB));
  player.SendCharList($112);
  player.SaveAccount;
end;

class function TPacketHandlers.Gates(var player: TPlayer; var buffer: array of Byte): Boolean;
var cX, cY : WORD;
    hour : TDateTime;
    teleport: TTeleport;
begin
	cX := ((player.Base.CurrentPosition.X) and $FFC);
	cY := ((player.Base.CurrentPosition.Y) and $FFC);

  for teleport in TeleportsList do
  begin
    if(teleport.Scr1.X = cX) and (teleport.Scr1.Y = cY) then
    begin
      if(player.Character.Base.Gold >= teleport.Price) then
      begin
        if(teleport.Time = -1) or (hour = teleport.Time) then
        begin
          if player.Base.Teleport(teleport.Dest1) then
          begin
            Dec(player.Character.Base.Gold, teleport.Price);
            player.RefreshMoney;
          end
          else
            player.SendClientMessage('Ocorreu um erro ao teleporta-lo');
				end
        else
          player.SendClientMessage('Hora incorreta para uso do Teleport');
			end
      else
        player.SendClientMessage('Gold Insuficiente');
      break;
    end;
  end;
	Result := true;
end;

class function TPacketHandlers.AddPoints(var player: TPlayer; var buffer: array of Byte): boolean;
var
  packet : TRequestAddPoints absolute buffer;
  onSuccess:boolean; max,reqclass,skillDiv,skillId,skillId2: integer;
  info: psmallint; master: pbyte; master2: pbyte; item: TItemList;
begin
  result:=true;
  case(packet.Mode)of
    0:
    begin
      if(player.Character.Base.pStatus <= 0)then
          exit;

      info:=@player.Character.Base.BaseScore.Str;
      inc(info,packet.Info);
      if(((packet.Info mod 2) = 0) and (info^ >= 32000)) or (((packet.Info mod 2) <> 0) and (info^ >= 12000))then
      begin
          player.SendClientMessage('M�ximo de pontos � 32.000');
          exit;
      end;

      inc(info^);
      dec(player.Character.Base.pStatus);

      player.Base.GetCurrentScore;
      player.SendEtc;
      player.Base.SendScore;
    end;
    1:
    begin
        if(player.Character.Base.pMaster <= 0)then
            exit;

        if((player.Character.Base.Learn and (128 shl (packet.Info * 8))) = 0)then
        begin
            max := (((player.Character.Base.BaseScore.Level + 1) * 3) shr 1);
            if(max > 200)then
                max := 200;
        end
        else
            max := 255;

        master := @player.Character.Base.BaseScore.wMaster;
        inc(master,packet.Info);
        if(master^ >= max)then
        begin
            player.SendClientMessage('M�ximo de pontos neste atributo.');
            exit;
        end;

        inc(master^);
        dec(player.Character.Base.pMaster);

        player.Base.GetCurrentScore;
        player.SendEtc;
        player.Base.SendScore;
    end;
    2:
    begin
        if(packet.Info < 5000) or (packet.Info > 5095)then
          exit;

        reqclass := TItemFunctions.GetEffectValue(packet.Info, EF_CLASS);

        if reqclass <= 2 then
          reqclass := reqclass - 1;

        if reqclass = 4 then
          reqclass := 2;

        if reqclass = 8 then
          reqclass := 3;

        skillDiv := Trunc(((packet.Info) - (5000 + (24 * reqclass)))/8) + 1;
        skillID := (packet.Info - 5000);
        skillID2 := (packet.Info - 5000) mod 24;

        item:= ItemList[packet.Info];

        info:=@item.STR;
        inc(info, skillDiv);
        master2 := @player.Character.Base.CurrentScore.wMaster;
        inc(master2, skillDiv);

        onSuccess := false;

        if((player.Character.Base.Learn and (1 shl skillID2)) = 0) then
            if(master2^ >= info^) then
                if(player.Character.Base.pSkill >= SkillsData[skillId].SkillPoint) then
                    if(player.Character.Base.Gold >= item.Price) then
                        if(player.Character.Base.BaseScore.Level >= item.Level) then
                            if(reqclass = player.Character.Base.ClassInfo) then
                                onSuccess := true
                            else player.SendClientMessage('N�o � poss�vel aprender Skills de outras classes.')
                        else player.SendClientMessage('Level insuficiente para adquirir a Skill.')
                    else player.SendClientMessage('Dinheiro insuficiente para adquirir a Skill.')
                else player.SendClientMessage('N�o h� Pontos de Skill suficientes.')
            else player.SendClientMessage('N�o h� Pontos de habilidade suficientes.')
        else player.SendClientMessage('Voc� j� aprendeu esta skill.');

        if(onSuccess = true) then
        begin
            player.Character.Base.Learn := player.Character.Base.Learn or (1 shl skillID2);
            dec(player.Character.Base.pSkill,SkillsData[skillid].SkillPoint);


            player.Base.GetCurrentScore;
            player.Base.SendScore;
            player.SendEtc;
        end;
    end;
  end;
end;

class function TPacketHandlers.MoveItem(var player: TPlayer; var buffer: array of Byte): Boolean;
var packet : TMoveItemPacket absolute buffer;
    destItem, srcItem: pItem; aux: TItem; pos: BYTE;  error: integer;
begin
  if(packet.destSlot < 0) or (packet.srcSlot < 0)then
    exit;
  {
  ZeroMemory(destItem, sizeof(TItem));
  ZeroMemory(srcItem, sizeof(TItem));
  }
  case(packet.DestType) of
    INV_TYPE:
    begin
      if(packet.destSlot < 64) then
        destItem := @player.Character.Base.Inventory[packet.destSlot]
      else exit;
    end;
    EQUIP_TYPE:
    begin
      if(packet.destSlot < 16) then
        destItem := @player.Character.Base.Equip[packet.destSlot]
      else exit;
    end;
    STORAGE_TYPE:
    begin
      if(packet.destSlot < 128) then
        destItem := @player.Account.Header.StorageItens[packet.destSlot]
      else exit;
    end;
  end;

  case(packet.srcType) of
    INV_TYPE:
    begin
      if(packet.srcSlot < 64) then
        srcItem := @player.Character.Base.Inventory[packet.srcSlot]
      else exit;
    end;
    EQUIP_TYPE:
    begin
      if(packet.srcSlot < 16) then
        srcItem := @player.Character.Base.Equip[packet.srcSlot]
      else exit;
    end;
    STORAGE_TYPE:
    begin
      if(packet.srcSlot < 128)then
        srcItem := @player.Account.Header.StorageItens[packet.srcSlot]
      else exit;
    end;
  end;

  error := 0;
  //if (packet.destType = INV_TYPE) then
  //begin
    //if not(TItemFunctions.CanCarry(player, srcItem^, packet.destSlot mod 9, packet.destSlot div 9, @error)) then
     // exit;
  //end;

  if (destItem.Index = 0) then
  begin
    Move(srcItem^, destItem^, 8);
  end
  else
  begin
    Move(destItem^, aux, 8);
    Move(srcItem^, destItem^, 8);
    Move(aux, srcItem^, 8);
  end;

  if (packet.destType = INV_TYPE) and (packet.srcSlot = 6) and not(player.Character.Base.Equip[7].Index = 0) then
  begin
    pos := TItemFunctions.GetEffectValue(player.Character.Base.Equip[7].Index, EF_POS);

		if(pos = 192) then
		begin
			Move(player.Character.Base.Equip[7], player.Character.Base.Equip[6], 8);
			ZeroMemory(@player.Character.Base.Equip[7], 8);
    end;
  end;

  if (packet.destType = EQUIP_TYPE) and (packet.destSlot = 14) then
    player.Base.GenerateBabyMob
  else
  begin
    if (packet.srcType = EQUIP_TYPE) and (packet.srcSlot = 14) then
      player.Base.UngenerateBabyMob(DELETE_UNSPAWN);
  end;

  ZeroMemory(srcItem,8);
  player.SendPacket(@packet, packet.Header.Size);
  player.Base.GetCurrentScore;
  player.Base.SendScore;
  player.SendEtc;
  player.Base.SendEquipItems(False);
  result := true;
end;

class function TPacketHandlers.DeleteItem(var player: TPlayer; var buffer: array of Byte) : Boolean;
var packet: TDeleteItem absolute buffer;
begin
  result:=false;
  if(packet.slot < 0) or (packet.slot > 60)then
    exit;
  if(player.Character.Base.Inventory[packet.slot].Index = packet.itemid)then
    ZeroMemory(@player.Character.Base.Inventory[packet.slot],8)
  else
    player.RefreshInventory;
  result:=true;
end;

class function TPacketHandlers.UngroupItem(var player: TPlayer; var buffer: array of Byte) : Boolean;
var packet: TGroupItem absolute buffer; i : BYTE; aux: TItem;
begin
  result:=false;
  if(packet.slot < 0) or (packet.slot > 60)then
    exit;
  if(player.Character.Base.Inventory[packet.slot].Index = packet.itemid)then
  begin
    for i := 0 to 2 do
    begin
      if (player.Character.Base.Inventory[packet.slot].Effects[i].Index = 61) then
      begin
        Move(player.Character.Base.Inventory[packet.slot], aux, 8);
        aux.Effects[i].Value := packet.quant;
        if (TItemFunctions.PutItem(player, aux) <> -1) then
        begin
          dec(player.Character.Base.Inventory[packet.slot].Effects[i].Value, packet.quant);
          player.RefreshInventory;
        end
        else
        begin
          player.SendClientMessage('Falta espa�o no invent�rio.');
          //player.RefreshInventory;
        end;
        break;
      end;
    end;
  end;
  //else
  //  RefreshInventory(index);
  result:=true;
end;


class function TPacketHandlers.SendNPCSellItens(var player: TPlayer; var buffer: array of Byte) : Boolean;
var packet: TSendNPCSellItensPacket absolute buffer;
    npc : TCharacter;
    npcId : WORD;
    i, j, k : Integer;
begin
  result := true;
  Move(buffer[12], npcId, 2);
  npc := Npcs[npcId].Character;

  if not(npc.Last.InRange(player.Character.Base.Last, 6)) then
    exit;

  packet.Header.Size := 236;
  packet.Header.Code := $17C;
  packet.Header.Index := 30000;

  Move(npc.Inventory, packet.Itens, SizeOf(TItem) * 26);

  case npc.Merchant of
    1://itens
    begin
      packet.Merch := 1;
      packet.Imposto := Taxes;
    end;
    19:
    begin
      packet.Merch := 3;
      {
      j:=0;
      for i := 0 to 64 do
      begin
        if(npc.Inventory[i].Index <> 0) and (j <= 26)then begin
          Move(npc.Inventory[i],packet.Itens[j],8);
          inc(j);
          if(j = 8) or (j = 17)then
            inc(j);
        end;
      end;
      }
      packet.Imposto := 0;
    end;
  end;
  player.SendPacket(@packet, packet.Header.Size);
end;

class function TPacketHandlers.Trade(var player: TPlayer; var buffer: array of Byte): Boolean;
var packet: TTradePacket absolute buffer;
    otherPlayer: TPlayer;
    i, j: Byte;
begin
  otherPlayer := Players[packet.OtherClientId];
	player.Character.Trade.Confirm := packet.Confirm;

	if not(otherPlayer.Base.IsActive) then
	begin
		player.SendClientMessage('Este jogador n�o est� conectado.');
		exit;
	end;

	if((otherPlayer.Character.PlayerKill) or (player.Character.PlayerKill))then
	begin
		player.SendClientMessage('Desative o modo pk.');
		exit;
	end;
	if(not(player.Character.Trade.isTrading)) and ((not(otherPlayer.Character.Trade.isTrading)) and (packet.TradeItem[0].Index <= 0))then
	begin
    ZeroMemory(@packet, sizeof(TTradePacket));
		packet.OtherClientid := player.Base.ClientId;
		packet.Header.index := otherPlayer.Base.ClientId;
		packet.Header.Size := sizeof(TTradePacket);
		packet.Header.Code := $383;
		for i := 0 to 14 do
    begin
			packet.TradeItemSlot[i] := -1;
      player.Character.Trade.TradeItemSlot[i] := -1;
    end;
		otherPlayer.SendPacket(@packet, packet.Header.Size);
		player.Character.Trade.Timer := now;
		player.Character.Trade.IsTrading := true;
		player.Character.Trade.Waiting := true;
		player.Character.Trade.otherClientid := otherPlayer.Base.ClientId;
		exit;
	end
  else
	begin
		if((not(player.Character.Trade.IsTrading))
      and (otherPlayer.Character.Trade.IsTrading)
      and (otherPlayer.Character.Trade.otherClientid = player.Base.ClientId)
			and (otherPlayer.Character.Trade.Waiting))then
		begin
			ZeroMemory(@packet, sizeof(TTradePacket));
	   	packet.OtherClientid := player.Base.ClientId;
	   	packet.Header.index := otherPlayer.Base.ClientId;
	   	packet.Header.Size := 156;
	   	packet.Header.Code := $383;
	   	for i := 0 to 14 do
      begin
	   		packet.TradeItemSlot[i] := -1;
        player.Character.Trade.TradeItemSlot[i] := -1;
      end;
	   	otherPlayer.SendPacket(@packet, packet.Header.Size);
	   	player.Character.Trade.Timer := now;
	   	player.Character.Trade.IsTrading := true;
	   	player.Character.Trade.Waiting := true;
	   	player.Character.Trade.otherClientid := otherPlayer.Base.ClientId;
      exit;
		end;

		if((player.Character.Trade.IsTrading)
      and (player.Character.Trade.otherClientid <> otherPlayer.Base.ClientId)) then
		begin
			player.SendClientMessage('Voc� j� est� em uma negocia��o.');
			player.Character.Trade.IsTrading := false;
			player.Character.Trade.Waiting := false;
			player.Character.Trade.otherClientid := 0;
			exit;
		end;

		if((otherPlayer.Character.Trade.IsTrading)
      and (otherPlayer.Character.Trade.otherClientid <> player.Base.ClientId)) then
		begin
			player.SendClientMessage('O outro jogador j� est� em uma negocia��o.');
			player.Character.Trade.IsTrading := false;
			player.Character.Trade.Waiting := false;
			player.Character.Trade.otherClientid := 0;
			exit;
		end;
	end;
	if((otherPlayer.Character.Trade.OtherClientid <> player.Base.ClientId)
    and (player.Character.Trade.otherClientid <> otherPlayer.Base.ClientId))then
	begin
		player.SendClientMessage('Ocorreu um erro.');
		otherPlayer.SendClientMessage('Ocorreu um erro.');
		player.CloseTrade;
		exit;
	end;
	if(player.Character.Trade.Confirm)then
	begin
		if((now - player.Character.Trade.Timer) < strtotime('00:00:02'))then
		begin
			player.SendClientMessage('Aguarde 2 segundos e aperte o bot�o.');
			player.Character.Trade.Confirm := false;
			player.Character.Trade.Timer := Now;
			exit;
		end
    else
		begin
			if(otherPlayer.Character.Trade.Confirm)then
			begin
        {
				if not(CheckItensTrade(player.Base.ClientId, 0)) then
				begin
					otherPlayer.SendClientMessage('Voc� n�o tem espa�o o suficiente no invent�rio.');
					player.SendClientMessage('O outro player n�o tem espa�o o suficiente no invent�rio.');
					player.CloseTrade;
					otherPlayer.CloseTrade;
					exit;
				end;
				if not(CheckItensTrade(otherPlayer.Base.ClientId, 0)) then
				begin
					player.SendClientMessage('Voc� n�o tem espa�o o suficiente no invent�rio.');
					otherPlayer.SendClientMessage('O outro player n�o tem espa�o o suficiente no invent�rio.');
					player.CloseTrade;
					otherPlayer.CloseTrade;
					exit;
				end;
        }
        {
				for i:=0 to 14 do
				begin
					if(thisclient->Trade.TradeItemSlot[i] > -1 && thisclient->Trade.TradeItemSlot[i] < 64)
						ZeroMemory(&thisclient->Inventory[thisclient->Trade.TradeItemSlot[i]],8);
					if(otherclient->Trade.TradeItemSlot[i] > -1 && otherclient->Trade.TradeItemSlot[i] < 64)
						ZeroMemory(&otherclient->Inventory[otherclient->Trade.TradeItemSlot[i]],8);
        end;

				for(BYTE i = 0; i < 15; i++)
				begin
					BYTE slot = GetFirstSlotSADD(Clientid,0,64);
					BYTE otherslot = GetFirstSlotSADD(otherClientid,0,64);
					if(slot != -1 && otherclient->Trade.Itens[i].Index != 0)
						memcpy(&thisclient->Inventory[slot],&otherclient->Trade.Itens[i],8);
					if(otherslot != -1 && thisclient->Trade.Itens[i].Index != 0)
					memcpy(&otherclient->Inventory[slot],&thisclient->Trade.Itens[i],8);
				end;
        }
				if(player.Character.Base.Gold + otherPlayer.Character.Trade.Gold <= 2000000000)
					and (otherPlayer.Character.Base.Gold + player.Character.Trade.Gold <= 2000000000)then
				begin
					inc(player.Character.Base.Gold, otherPlayer.Character.Trade.Gold);
					inc(otherPlayer.Character.Base.Gold, player.Character.Trade.Gold);
					dec(player.Character.Base.Gold, player.Character.Trade.Gold);
					dec(otherPlayer.Character.Base.Gold, otherPlayer.Character.Trade.Gold);
          player.RefreshMoney;
          otherPlayer.RefreshMoney;
				end
				else
				begin
					player.SendClientMessage('Limite de 2 Bilh�es de gold.');
					otherPlayer.SendClientMessage('Limite de 2 Bilh�es de gold.');
					player.CloseTrade;
					otherPlayer.CloseTrade;
					exit;
				end;
				//CheckItensTrade(player.Base.ClientId, 1);
				//CheckItensTrade(otherPlayer.Base.ClientId, 1);
				player.CloseTrade;
				otherPlayer.CloseTrade;
				exit;

			end
      else
			begin
        if (CompareMem(@packet.TradeItem[0],@player.Character.Trade.Itens[0],8*15) <> true) or (packet.Gold <> player.Character.Trade.Gold)
            or (CompareMem(@packet.TradeItemSlot[0],@player.Character.Trade.TradeItemSlot[0],15) <> true)then
        begin
          player.Character.Trade.Confirm := False;
          otherPlayer.Character.Trade.Confirm := False;
        end;
        player.Character.Trade.Gold := packet.Gold;
        Move(packet.TradeItem[0],player.Character.Trade.Itens[0],8*15);
        Move(packet.TradeItemSlot[0],player.Character.Trade.TradeItemSlot[0],15);
				ZeroMemory(@packet, sizeof(TTradePacket));
        Move(player.Character.Trade.Itens[0],packet.TradeItem[0],8*15);
				Move(player.Character.Trade.TradeItemSlot[0],packet.TradeItemSlot[0],15);
        packet.Gold:= player.Character.Trade.Gold;
        packet.OtherClientid := player.Base.ClientId;
        packet.Header.index := otherPlayer.Base.ClientId;
        packet.Header.Size := 156;
        packet.Header.Code := $383;
        for i := 0 to 14 do
          packet.TradeItemSlot[i] := -1;
        packet.Confirm := player.Character.Trade.Confirm;
				otherPlayer.SendPacket(@packet, packet.Header.Size);
			end;
      exit;
		end;
	end
  else
	begin
		//Recebe os itens e gold para o buffer Trade
		if(packet.Gold > player.Character.Base.Gold)then
		begin
			player.SendClientMessage('Ocorreu um erro.');
			otherPlayer.SendClientMessage('Ocorreu um erro.');
			player.CloseTrade;
			otherPlayer.CloseTrade;
			exit;
		end;
		for i := 0 to 14 do
		begin
			if(packet.TradeItemSlot[i] <> -1)then
			begin
				for j := i+1 to 14 do
				begin
					if((packet.TradeItemSlot[i] = packet.TradeItemSlot[j]) and (i <> j))then
					begin
						player.SendClientMessage('Ocorreu um erro.');
            otherPlayer.SendClientMessage('Ocorreu um erro.');
            player.CloseTrade;
            otherPlayer.CloseTrade;
            exit;
					end;
				end;
				if(packet.TradeItemSlot[i] < -1) or (packet.TradeItemSlot[i] > 63)then
        begin
					player.SendClientMessage('Ocorreu um erro.');
          otherPlayer.SendClientMessage('Ocorreu um erro.');
          player.CloseTrade;
          otherPlayer.CloseTrade;
          exit;
				end;
				if((CompareMem(@packet.TradeItem[i],@player.Character.Base.Inventory[packet.TradeItemSlot[i]],8) <> true)
					and (packet.TradeItem[i].Index <> 0)) then
				begin
					player.SendClientMessage('Ocorreu um erro.');
          otherPlayer.SendClientMessage('Ocorreu um erro.');
          player.CloseTrade;
          otherPlayer.CloseTrade;
          exit;
				end;
			end
      else
			begin
				if(packet.TradeItem[i].Index > 0) or
					(player.Character.Trade.Itens[i].Index > 0)then
				begin
					player.SendClientMessage('Ocorreu um erro.');
          otherPlayer.SendClientMessage('Ocorreu um erro.');
          player.CloseTrade;
          otherPlayer.CloseTrade;
          exit;
				end;
			end;
		end;
    if (CompareMem(@packet.TradeItem[0],@player.Character.Trade.Itens[0],8*15) <> true) or (packet.Gold <> player.Character.Trade.Gold)
        or (CompareMem(@packet.TradeItemSlot[0],@player.Character.Trade.TradeItemSlot[0],15) <> true)then
    begin
      player.Character.Trade.Confirm := False;
      otherPlayer.Character.Trade.Confirm := False;
    end;
		player.Character.Trade.Gold := packet.Gold;
    Move(packet.TradeItem[0],player.Character.Trade.Itens[0],8*15);
    Move(packet.TradeItemSlot[0],player.Character.Trade.TradeItemSlot[0],15);
    ZeroMemory(@packet, sizeof(TTradePacket));
    packet.OtherClientid := player.Base.ClientId;
    packet.Header.index := otherPlayer.Base.ClientId;
    packet.Header.Size := 156;
    packet.Header.Code := $383;
    packet.Confirm := player.Character.Trade.Confirm;
    Move(player.Character.Trade.Itens[0],packet.TradeItem[0],8*15);
    Move(player.Character.Trade.TradeItemSlot[0],packet.TradeItemSlot[0],15);
    packet.Gold:= player.Character.Trade.Gold;
    player.Character.Trade.Timer := now;
		player.Character.Trade.Confirm := false;
	  otherPlayer.SendPacket(@packet, packet.Header.Size);
		end;
	exit;
end;


class function TPacketHandlers.BuyNpcItens(var player: TPlayer; var buffer: array of Byte): Boolean;
var packet: TBuyNpcItensPacket absolute buffer;
    item : TItem;
    price : Integer;
    slot: BYTE;
    adjustedPrice: integer;
begin
/////////////////////////////////
  if(packet.sellSlot > 8) then Dec(packet.sellSlot, 18);
////////////////////////////////
  item  := Npcs[packet.mobID].Character.Inventory[packet.sellSlot];
  if(item.Index = 0) then
    exit;

  price := Trunc(ItemList[item.Index].Price + (ItemList[item.Index].Price*(Taxes/100)));

  if(player.Character.Base.Gold >= price) then
  begin
    slot := TItemFunctions.GetEmptySlot(player);
    if(slot <> 254) then
    begin
      adjustedPrice := price + Trunc((price * Taxes) / 100);
      Dec(player.Character.Base.Gold, adjustedPrice);
      player.SendCreateItem(INV_TYPE, slot, item);
      Move(item, player.Character.Base.Inventory[slot], sizeof(TItem));
      player.SaveAccount;
      player.RefreshMoney;
    end
    else player.SendClientMessage('Invent�rio cheio. Imposs�vel Negociar.');
  end
  else player.SendClientMessage('Gold insuficiente.');
end;


class function TPacketHandlers.BuyStoreTrade(var player: TPlayer; var buffer: array of Byte): Boolean;
var
  packet: TBuyStoreItemPacket absolute buffer;
  seller : TPlayer;
  emptySlot : Byte;
  response : TSendItemBoughtPacket;
  i : BYTE;
  find : Boolean;
  branco : TItem;
begin
    result := true;
    if(packet.Slot > 11) or
      (packet.SellerId = player.Base.ClientId) or
      (not(Players[packet.SellerId].Character.IsStoreOpened)) then
    begin
      exit;
    end;

    seller := Players[packet.SellerId];
    if not(seller.Base.IsActive) then exit;

    if(packet.Gold <> seller.Character.TradeStore.Gold[packet.Slot])then
    begin
      player.SendClientMessage('Item Price changed error.');
      exit;
    end;

    if not(CompareMem(@seller.Character.TradeStore.Item[packet.Slot], @packet.Item, 8)) then
    begin
      player.SendClientMessage('Item changed error.');
      exit;
    end;

    if(player.Character.Base.Gold < seller.Character.TradeStore.Gold[packet.Slot])then
    begin
      player.SendClientMessage('N�o pussui essa quantia.');
      exit;
    end;

    if((seller.Character.TradeStore.Gold[packet.Slot] + seller.Account.Header.StorageGold) > 2000000000)then
    begin
      player.SendClientMessage('O vendedor atingiu o limite de 2.000.000.000 de gold.');
      exit;
    end;

    if not(TItemFunctions.PutItem(player, packet.Item) = -1) then
    begin
      player.SendClientMessage('Sem espa�o no invent�rio.');
      exit;
    end;

    //Move(player.Character.Base.Inventory[emptySlot], packet.Item, 8);
    //player.SendCreateItem(INV_TYPE, emptySlot, packet.Item);

    Dec(player.Character.Base.Gold, packet.Gold);
    player.RefreshMoney;
    Inc(seller.Account.Header.StorageGold, packet.Gold);

    response.Header.Size := sizeof(TSendItemBoughtPacket);
    response.Header.Code := $39B;
    response.Header.Index := $7530;
    response.SellerId := packet.SellerId;
    response.Slot := packet.Slot;

    // Apaga o item na auto-venda para todos os clients visiveis
    seller.Base.SendToVisible(@response, response.Header.Size);

    ZeroMemory(@seller.Account.Header.StorageItens[seller.Character.TradeStore.Slot[packet.Slot]], 8);
    seller.SendCreateItem(STORAGE_TYPE, seller.Character.TradeStore.Slot[packet.Slot], branco);

    seller.SendSignal($7530, $339, seller.Account.Header.StorageGold);

    seller.Character.TradeStore.Gold[packet.Slot] := 0;
    seller.Character.TradeStore.Slot[packet.Slot] := 0;
    ZeroMemory(@seller.Character.TradeStore.Item[packet.Slot],8);
    seller.SendClientMessage('Um produto foi vendido.');

    find := false;
    for i := 0 to 11 do
      if (seller.Character.TradeStore.Item[i].Index <> 0) then
        find := true;

    if not(find) then
    begin
      if (seller.Character.IsStoreOpened) then
      begin
        seller.SendSignal(seller.Base.ClientId, $384);
        seller.Character.IsStoreOpened := False;

        seller.Base.SendCreateMob(SPAWN_NORMAL);
      end;
    end;

    result:= true;
end;

class function TPacketHandlers.SellItemsToNPC(var player: TPlayer; var buffer: array of Byte) : Boolean;
var packet : TSellItemsToNpcPacket absolute buffer;
    item : TItem;
    price, tax : integer;
begin
  if(packet.invType <> INV_TYPE)then
  begin
    player.SendClientMessage('Passe o item para o invent�rio.');
    exit;
  end;

  if(packet.invSlot > 63) then
    exit;

  item := player.Character.Base.Inventory[packet.invSlot];
  price := TItemFunctions.GetItemAbility(item, EF_PRICE);

  if(player.Character.Base.Gold + price <= 2000000000)then
  begin
    if(price mod 20000 = 0)then
      price := Trunc((price / (4 + (Round((price / 20000) - 1) * 2))))
    else
      price := Trunc((price / (4 + (Round(price / 20000) * 2))));
    tax := Trunc((price * Taxes) / 100);
    Inc(player.Character.Base.Gold, price - tax);

    ZeroMemory(@player.Character.Base.Inventory[packet.invSlot].Index, 8);
    player.SaveAccount;
    player.RefreshInventory;
  end
  else player.SendClientMessage('Limite de 2 Bilh�es de gold.');
end;



class function TPacketHandlers.SelectCharacter(var player: TPlayer; var buffer: array of Byte): Boolean;
var packet : TSelectCharacterRequestPacket absolute buffer;
begin
  if (packet.CharacterId < 0) or (packet.CharacterId > 3) then begin
    player.SendClientMessage('Posi��o Inv�lida.');
    exit;
  end;

  if(not TFunctions.FindCharacter(player.Account.Characters[packet.CharacterId].Base.Name)
  OR (player.Account.Characters[packet.CharacterId].Base.Equip[0].Index = 0)) then
  begin
    player.SendClientMessage('Personagem Inv�lido.');
    exit;
  end;
  player.SendToWorld(packet.CharacterId);
  //exit;
end;



class function TPacketHandlers.PlayerChat(var player: TPlayer; var buffer: array of Byte): Boolean;
var packet : TChatPacket absolute buffer;
begin
  player.Base.SendToVisible(@packet, packet.Header.Size);
end;

class function TPacketHandlers.RequestParty(var player: TPlayer; var buffer: array of Byte): boolean;
var packet: TPartyRequestPacket absolute buffer;
    party : TParty;
begin
  result := true;
  if(packet.TargetId < 1) or (packet.TargetId > 1000) or (packet.SenderId <> player.Base.ClientId) then
  begin
    exit;
  end;

  party := Parties[player.Base.PartyId];
  if(party.Leader = 0) then
  begin
    player.SendClientMessage('Grupo n�o encontrado.');
    exit;
  end;

  if(party.Leader <> player.Base.ClientId){ and (party.Leader <> 0)} then
  begin
    player.SendClientMessage('Voc� n�o � l�der. Saia do grupo para criar um novo grupo.');
    exit;
  end;
  if(Players[packet.TargetId].Base.PartyId <> 0)then
  begin
    player.SendClientMessage('O outro jogador j� est� em um grupo.');
    exit;
  end;
  if(packet.Level <> player.Character.Base.CurrentScore.Level)
    or (packet.MaxHp <> player.Character.Base.CurrentScore.MaxHP)
    or (packet.CurHp <> player.Character.Base.CurrentScore.CurHP) then
  begin
    result := false;
    exit;
  end;
  if(AnsiCompareStr(player.Character.Base.Name, packet.Nick) <> 0)then
  begin
    result := false;
    exit;
  end;
  if(Players[packet.TargetId].Base.PartyId = 0)then
  begin
    Players[packet.TargetId].Base.PartyId := player.Base.ClientId;
  end;
  Players[packet.TargetId].SendPacket(@packet, packet.Header.Size);
end;

class function TPacketHandlers.ExitParty(var player: TPlayer; var buffer: array of Byte): boolean;
var packet: TExitPartyPacket absolute buffer;
    //leader : TPlayer;
    i: BYTE;
    find: boolean;
    party : TParty;
    //id,liderid, exitid: Word; i: BYTE; find: boolean;
begin
  result := true;
  find   := false;
  if(packet.Header.Index <> player.Base.ClientId) then
    exit;
  //liderid := player.Base.Party.Leader;
  //leader := Players[player.Base.Party.Leader];

  if(player.Base.PartyId = 0) then
  begin
    player.SendClientMessage('Voc� n�o est� em um grupo.');
    exit;
  end;

  if(player.Base.PartyId = player.Base.ClientId) and (packet.ExitId = 0) then
  begin
    player.SendExitParty(player.Base.PartyId, player.Base.PartyId, 0);
    exit;
  end;

  //Parties.TryGetValue(leader.Base.ClientId, party);
  party := Parties[player.Base.PartyId];
  if(party.Leader = 0) then
  begin
    player.SendClientMessage('Grupo n�o encontrado.');
    exit;
  end;

  if(packet.ExitId <> 0) then
  begin
    //lider mandou tirar da pt
    if(player.Base.ClientId <> player.Base.PartyId) then
    begin
      player.SendClientMessage('Somente o lider pode expulsar membros do grupo.');
      exit;
    end
    else
    begin
      for i := 0 to 10 do
        if (party.Members[i] > 0) and (party.Members[i] < 1000) then
        begin
          find := true;
          Players[party.Members[i]].SendExitParty(player.Base.PartyId, party.Members[packet.ExitId], 0);
        end;
        party.Members[packet.ExitId] := 0;
    end;
  end
  else
  begin
    if(player.Base.ClientId <> player.Base.PartyId) then
    begin
      //Membro Saiu da Pt
      for i := 0 to 10 do
      begin
        if (party.Members[i] > 0) and (party.Members[i] <> player.Base.ClientId)
        and (party.Members[i] < 1000) then
        begin
          find := true;
          Players[party.Members[i]].SendExitParty(player.Base.PartyId, player.Base.ClientId, 0);
        end
        else
          if (party.Members[i] = player.Base.ClientId) then
            party.Members[i] := 0;
      end;
    end
    else
    begin
      //Lider Saiu da Pt
    end;
  end;
  Players[player.Base.PartyId].SendExitParty(player.Base.PartyId, player.Base.ClientId, 0);

  if not(find) then
    Players[player.Base.PartyId].SendExitParty(player.Base.PartyId, player.Base.PartyId, 0);
end;

class function TPacketHandlers.AcceptParty(var player: TPlayer; var buffer: array of Byte): boolean;
var packet: TAcceptPartyPacket absolute buffer;
    party : PParty;
    //leader : TPlayer;
    i: BYTE; id: WORD; first: boolean;
    leader: TPlayer;
begin
  result:=true;
  if(packet.LeaderId <> player.Base.PartyId) then
    exit;

  if not(TPlayer.GetPlayer(packet.LeaderId, leader)) then
    exit;

  if(AnsiCompareStr(leader.Character.Base.Name, packet.Nick) <> 0)then
    exit;
  first := false;

  party := @Parties[leader.Base.PartyId];

  if(party.Leader = 0) then
  begin
    party.Leader := packet.LeaderId;
    first := true;
  end;

  if not(party.AddMember(player.Base.ClientId)) then
  begin
    player.SendClientMessage('O grupo est� cheio.');
    exit;
  end;
//  Parties[player.Base.ClientId].Leader := party.Leader;
//  party.Members[i]                     := player.Base.ClientId;
  //party.Leader     := leader.Base.ClientId;

  //eviar os membros do grupo e o lider para o novo membro
  //para enviar o lider para o novo membro temos q enviar o Clientid com o index do lider
  //e o LeaderId com o clientid do lider
  if(first) then //enviar lider para o lider quando o grupo � formado
  begin
    //leader.Base.Party.Leader := leader.Base.ClientId;
    leader.Base.SendParty(packet.LeaderId, packet.LeaderId);
  end;
  leader.Base.SendParty(256, player.Base.ClientId);
  player.Base.SendParty(packet.LeaderId, packet.LeaderId);


  player.Base.SendParty; // Envia os membros pra esse player,
                         // E o player para os outros membros
end;

class function TPacketHandlers.SendClientSay(var player: TPlayer; var buffer: array of Byte): Boolean;
var packet : TClientMessagePacket absolute buffer; msg: string;
begin
    packet.Message[95] := #0;

    if(AnsiCompareStr(Trim(packet.Message),'guild') = 0)then
    begin
        player.SendClientMessage('Ainda n�o implementado.');
        result := true;
        exit;
    end;

    msg := packet.Message[0] + packet.Message;
    player.Base.SendChat(msg);
    result:=true;
end;

class function TPacketHandlers.CargoGoldToInventory(var player: TPlayer; var buffer: array of Byte): Boolean;
var packet : TRefreshMoneyPacket absolute buffer;
begin
  if(packet.Gold > player.Account.Header.StorageGold)then
  begin
    player.SendClientMessage('Voc� n�o possui essa quantia.');
    exit;
  end;
  if(player.Character.Base.Gold + packet.Gold > 2000000000)then
  begin
    player.SendClientMessage('Limite de 2 bilh�es de gold.');
    exit;
  end;
  inc(player.Character.Base.Gold,packet.Gold);
  dec(player.Account.Header.StorageGold,packet.Gold);
  player.SaveAccount;
  player.SendPacket(@packet, packet.Header.Size);
  //player.SendSignal(30002, $387, packet.Gold);
end;

class function TPacketHandlers.InventoryGoldToCargo(var player: TPlayer; var buffer: array of Byte): Boolean;
var packet : TRefreshMoneyPacket absolute buffer;
begin
  if(packet.Gold > player.Character.Base.Gold)then
  begin
    player.SendClientMessage('Voc� n�o possui essa quantia.');
    exit;
  end;
  if(player.Account.Header.StorageGold + packet.Gold > 2000000000)then
  begin
    player.SendClientMessage('Limite de 2 bilh�es de gold.');
    exit;
  end;
  inc(player.Account.Header.StorageGold,packet.Gold);
  dec(player.Character.Base.Gold,packet.Gold);
  player.SaveAccount;
  player.SendPacket(@packet, packet.Header.Size);
end;

class function TPacketHandlers.CloseTrade(var player: TPlayer): Boolean;
begin
  if (player.Character.IsStoreOpened) then
  begin
    player.Character.IsStoreOpened := False;

    player.Base.SendCreateMob(SPAWN_NORMAL);
    exit;
  end;
end;

class function TPacketHandlers.OpenStoreTrade(var player: TPlayer; var buffer: array of Byte): boolean;
var packet : TOpenTrade absolute buffer;
begin

  if(Players[packet.OtherClientId].Character.IsStoreOpened = false)then
  begin
    result:= true;
    exit;
  end;

  Players[packet.OtherClientId].SendAutoTrade(player);
  result:=true;
end;

class function TPacketHandlers.PKMode(var player: TPlayer; var buffer: array of Byte): boolean;
var packet : TSignalData absolute buffer;
begin
  if packet.Data > 255 then
  begin
    result := false;
    exit;
  end;

  player.Character.PlayerKill := IFThen(packet.Data = 1);
  result := True;
end;

class function TPacketHandlers.ChangeSkillBar(var player: TPlayer; var buffer: array of Byte): boolean;
var packet : TSkillBarChange absolute buffer;
begin
  result := False;
  Move(packet.SkillBar[0], player.Character.Base.SkillBar1[0],4);
  Move(packet.SkillBar[4], player.Character.Base.SkillBar2[0],16);
  result := True;
end;

class function TPacketHandlers.DropItem(var player: TPlayer; var buffer: array of Byte): boolean;
var packet : TReqDropItem absolute buffer; item : PItem; initItem : TInitItem; Pos : TPosition;
    initId : WORD;
begin
  result := False;
  if not(TItemFunctions.GetItem(item, player.Base, packet.invSlot, packet.invType))then
    exit;

  if(item.Index = 0)then
    exit;

  initId := TItemFunctions.FreeInitItem();
  if(initId = 0) then
    exit;

  Pos := player.Character.Base.Last;
  if not(TItemFunctions.GetEmptyItemGrid(Pos))then
  begin
    player.SendClientMessage('Sem espa�o para jogar items.');
    result := true;
  end;

  Move(item^, initItem.Item, sizeof(TItem));
  player.Base.RemoveItem(packet.invSlot, packet.invType);

  initItem.Pos := Pos;
  initItem.ClientId := player.Base.ClientId;
  initItem.TimeDrop := Now;
  InitItems[initId] := initItem;
  player.KnowInitItems.Add(initId);
  ItemGrid[Pos.Y][Pos.X] := initId;


  player.SendCreateDropItem(initId);

  player.SendDeleteItem(packet.invType, packet.InvSlot);

  result := true;
end;

class function TPacketHandlers.PickItem(var player: TPlayer; var buffer: array of Byte): boolean;
var packet : TReqPickItem absolute buffer; item : PItem; initItem : TInitItem; Pos : TPosition;
    initId : WORD;
begin
  result := False;

  if((packet.initId-10000) <> ItemGrid[packet.Position.Y][packet.Position.X])then
    exit;

  try
    initItem := InitItems[packet.initId - 10000];
  except
    exit;
  end;

  if(initItem.Item.Index = 0)then
    exit;

  TItemFunctions.PutItem(player, initItem.Item);
  ItemGrid[initItem.Pos.Y][initItem.Pos.X] := 0;
  ZeroMemory(@InitItems[packet.initId-10000], sizeof(TInitItem));
  player.KnowInitItems.Remove(packet.initId-10000);

  player.SendDeleteDropItem(packet.initId);

  result := true;
end;

class function TPacketHandlers.MovementCommand(var player: TPlayer; var buffer: array of Byte): Boolean;
var packet : TMovementPacket absolute buffer;
  dir : AnsiChar;
  cmm: Boolean;
begin
  if not(packet.Destination.IsValid) OR (packet.MoveType <> MOVE_NORMAL) then
  begin
    result := false;
    exit;
  end;
  if(player.Base.CurrentPosition = packet.Destination) then
  begin
    result := true;
    exit;
  end;
  if packet.Destination.Distance(player.Base.CurrentPosition) > 30 then
  begin
    result := true;
    exit;
  end;
  if (player.Character.Base.ClassInfo = 3) then
    player.Base.CleanAffect(HT_INIVISIBILIDADE);

  {
    Verificar �reas de guild e o campo de treinamento
  }

  if not TFunctions.UpdateWorld(player.Base.ClientId, packet.Destination, WORLD_MOB) then
  begin
    result := true;
    exit;
  end;

  player.Base.Character.Last := player.Base.CurrentPosition;
  cmm := IfThen(packet.Header.Code = $367);

  if cmm then
  begin
    packet := TFunctions.GetAction(player.Base, packet.Destination, MOVE_NORMAL, @packet.Command);
  end
  else
  begin
    packet := TFunctions.GetAction(player.Base, packet.Destination, MOVE_NORMAL, nil);
  end;
  player.Base.IsDirty := true;
  player.base.SetDestination(packet.Destination);
  player.Base.SendToVisible(@packet, packet.Header.Size, false);
	if (cmm) then
	begin
		player.SendPacket(@packet, packet.Header.Size);
  end;
end;

end.
