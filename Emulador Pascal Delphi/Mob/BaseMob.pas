unit BaseMob;

interface

uses Windows, PlayerData, MiscData, Packets, Generics.Collections, SysUtils,
  DateUtils, Diagnostics;

type TPrediction = record
  ETA: Single;
  Timer: {TDateTime;}TStopwatch;
  Source: TPosition;
  Destination: TPosition;

  function CanPredict: Boolean;
  function Elapsed: Integer;
  function Delta: Single;
  function Interpolate(out d: Single): TPosition;

  procedure Create; overload;
  procedure CalcETA(speed: Byte);
end;

type PBaseMob = ^TBaseMob;
TBaseMob = record
  private
    _prediction: TPrediction;
    _cooldown: TDictionary<Byte, TTime>;
    _currentPosition: TPosition;

    procedure DeterminMoveDirection(const pos: TPosition);

    procedure AddToVisible(var mob : TBaseMob);
    procedure RemoveFromVisible(mob : TBaseMob);

    procedure SendSignal(pIndex, opCode: WORD);
    function  GetEquipDamage(LR : Integer): WORD;
    procedure ApplyDamage(attacker: TBaseMob; damage: Integer);

  public
    ClientId : WORD;
    Character : PCharacter;
    AttackSpeed : WORD;
    IsActive : Boolean;
    IsDirty : Boolean;
    Mobbaby : WORD;
    PartyId : WORD;
    PartyRequestId : WORD;
    VisibleMobs : TList<WORD>;
    Target: PBaseMob;


    procedure Create(characterPointer : PCharacter; index: WORD); overload;
    procedure Destroy();
    procedure SendPacket(packet : pointer; size : WORD);


    property LeftDamage  : WORD Index 7 read GetEquipDamage;
    property RightDamage : WORD Index 8 read GetEquipDamage;

    function IsPlayer : boolean;
    function IsDead : boolean;
    function IsMoving : boolean;
    function InBattle : boolean;
    function CurrentPosition: TPosition;

    procedure SetDestination(const destination: TPosition);

    procedure UpdateVisibleList();

    function CheckCooldown(skillId: Byte): Boolean;
    procedure UsedSkill(skillId: Byte);

    // Sends
    procedure SendMovement(destination : TPosition; calcDirection: Boolean = true); overload;
    procedure SendMovement(destX, destY : SmallInt; calcDirection: Boolean = true); overload;
    procedure SendRemoveMob(delType: integer = 0; sendTo : WORD = 0);
    procedure SendChat(str: AnsiString);
    procedure SendCurrentHPMP();
    procedure SendScore();
    procedure SendCreateMob(spawnType : WORD = 0; sendTo : WORD = 0);
    procedure SendEmotion(effType, effValue: smallint);
    procedure SendToVisible(packet : pointer; size : WORD; sendToSelf : Boolean = true);
    procedure SendParty(leader, member : WORD); overload;
    procedure SendParty; overload;
    procedure SendDamage(target: TBaseMob; skillId : Byte; damage: Integer = -1); overload;
    procedure SendDamage(targets: TList<WORD>; skillId : Byte; damage: Integer = -1); overload;
    procedure SendMobDead(killer: TBaseMob);
    procedure SendAffects();
    procedure SendEquipItems(sendSelf : Boolean = True);

    //Gets
    procedure GetCurrentScore();
    procedure GetAffectScore;
    function GetFirstSlot(itemId: WORD; invType: BYTE): Integer;
    function GetItemAmount(itemId: Integer; inv: array of TItem): TItemAmount;
    function GetCurrentHP(): Integer;
    function GetCurrentMP(): Integer;
    function GetEmptySlot(): Byte;
    procedure GetCreateMob(out packet : TSendCreateMobPacket);
    function GetMaxAbility(eff: integer): integer;
    function GetMobAbility(eff: integer):integer;
    function GetDamage(target: TBaseMob; master: Byte): smallint;


    function Teleport(x, y : SmallInt) : Boolean; overload;
    function Teleport(position : TPosition) : Boolean; overload;

    procedure AddAffect(affect: TAffect);
    procedure SetAffect(affectId: Byte; affect: TAffect);
    procedure CleanAffect(affectId: Byte);

    procedure RemoveItem(slot, slotType: BYTE);
    procedure AddExp(exp : WORD);


    procedure GenerateBabyMob;
    procedure UngenerateBabyMob(ungenEffect: WORD);

    procedure ForEachInRange(range: Byte; proc: TProc<TPosition, TBaseMob, TBaseMob>); overload;
    procedure ForEachVisible(proc: TProc<TBaseMob>);


    class function GetMob(index: WORD; out mob: TBaseMob): boolean; overload; static;
    class function GetMob(index: WORD; out mob: PBaseMob): boolean; overload; static;
    class function GetMob(pos: TPosition; out mob: TBaseMob): boolean; overload; static;
    class procedure ForEachInRange(pos: TPosition; range: Byte; proc: TProc<TPosition, TBaseMob>); overload; static;
end;

const HPIncrementPerLevel: array[0..3] of integer = (
  3, // Transknight
  1, // Foema
  1, // BeastMaster
  2  // Hunter
);

const MPIncrementPerLevel: array[0..3] of integer = (
  1, // Transknight
  3, // Foema
  2, // BeastMaster
  1  // Hunter
);

implementation

uses GlobalDefs, Player, ItemFunctions, Functions, NPC, Log, Util, BuffsData;


function TBaseMob.CheckCooldown(skillId: Byte): Boolean;
begin
  Result := true;
  if not(_cooldown.ContainsKey(skillId)) then
    exit;

  if SecondsBetween(_cooldown[skillId], Now) < SkillsData[skillId].Delay then
  begin
    Result := false;
  end
  else
  begin
    _cooldown.Remove(skillId);
  end;
end;

procedure TBaseMob.CleanAffect(affectId: Byte);
var affect: TAffect;
begin
  ZeroMemory(@affect, sizeof(TAffect));
  SetAffect(affectId, affect);
end;

procedure TBaseMob.Create(characterPointer : PCharacter; index: WORD);
begin
  ZeroMemory(@self, sizeof(TBaseMob));
  VisibleMobs := TList<WORD>.Create;
  IsActive := true;
  IsDirty := false;
  Character := characterPointer;
  ClientId := index;

  _prediction.Create;
  _cooldown := TDictionary<Byte, TTime>.Create;
end;

procedure TBaseMob.Destroy();
var
  mob: TBaseMob;
begin
  self.IsActive := false;
  if(Character <> nil) then
    MobGrid[CurrentPosition.Y][CurrentPosition.X] := 0;
end;


function TBaseMob.GetEquipDamage(LR : Integer): WORD;
begin
  result := TItemFunctions.GetItemAbility(Character.Equip[LR], EF_DAMAGE) +
                 TItemFunctions.GetItemAbility(Character.Equip[LR],EF_DAMAGE2)+
                 TItemFunctions.GetItemAbility(Character.Equip[LR],EF_DAMAGEADD);
end;

function TBaseMob.GetFirstSlot(itemId: WORD; invType: BYTE): Integer;
var inv: TList<TItem>;
  i: BYTE;
begin
  inv := TList<TItem>.Create;

  case invType of
    EQUIP_TYPE: inv.AddRange(Character.Equip);
    INV_TYPE: inv.AddRange(Character.Inventory);
    STORAGE_TYPE:
    begin
      if not(IsPlayer) then
      begin
        Result := -1;
        exit;
      end;
      inv.AddRange(Players[ClientId].Account.Header.StorageItens);
    end;
  end;

  for i := 0 to inv.Count do
  begin
    if(inv[i].Index = itemId) then
    begin
      Result := i;
      exit;
    end;
  end;
end;

procedure TBaseMob.SendPacket(packet : pointer; size : WORD);
begin
  Server.SendPacketTo(clientId, packet, size);
end;

procedure TBaseMob.SendParty;
var party: PParty;
  i: WORD;
  member: TBaseMob;
begin
  if(PartyId = 0) then
    exit;

  party := @Parties[PartyId];

  for i in party.Members do
  begin
    if not(GetMob(i, member)) OR (i = ClientId) then
    begin
      party.Members.Remove(i);
      continue;
    end;

    if member.IsPlayer then
      member.SendParty(255 + party.Leader, ClientId);

    if self.IsPlayer then
      SendParty(255 + party.Leader, member.ClientId);
  end;
end;

procedure TBaseMob.SendParty(leader, member: WORD);
var packet: TSendPartyMember;
other : TBaseMob;
begin
  if not(self.IsPlayer) then exit;

  if not(GetMob(member, other)) then
    exit;

  packet.Header.Size := sizeof(TSendPartyMember);
  packet.Header.Code := $37D;
  packet.Header.Index := $7530;
  packet.unk2 := 52428;
  packet.LiderID := leader;
  packet.ClientId := member;

  packet.Level:= other.Character.CurrentScore.Level;
  packet.MaxHp:= other.Character.CurrentScore.MaxHP;
  packet.CurHp:= other.Character.CurrentScore.CurHP;
  Move(other.Character.Name[0],packet.Nick[0],16);

  SendPacket(@packet,packet.Header.Size);
end;

procedure TBaseMob.SendSignal(pIndex, opCode: WORD);
begin
  Server.SendSignalTo(ClientId, pIndex, opCode);
end;

procedure TBaseMob.SendToVisible(packet : pointer; size : WORD; sendToSelf : Boolean);
var i: WORD;
  player: TPlayer;
begin
  sendToSelf := IfThen(sendToSelf, IsPlayer, false);

  if(sendToSelf) then
    SendPacket(packet, size);

  for i in VisibleMobs do
  begin
    if(TPlayer.GetPlayer(i, player)) then
      player.SendPacket(packet, size, not sendToSelf);
  end;
end;

procedure TBaseMob.SendRemoveMob(delType: integer = DELETE_NORMAL; sendTo : WORD = 0);
var packet: TSignalData;
  mob: TBaseMob;
  i: WORD;
begin
  packet.Header.Size := sizeof(TSignalData);
  packet.Header.Code := $165;
  packet.Header.Index := self.ClientId;

  packet.Data := delType;
  if(sendTo = 0) then
    SendToVisible(@packet, packet.Header.Size)
  else
    Server.SendPacketTo(sendTo, @packet, packet.Header.Size);

  for i in VisibleMobs do
  begin
    if(GetMob(i, mob)) then
      RemoveFromVisible(mob);
  end;

  VisibleMobs.Clear;
end;

procedure TBaseMob.SendMobDead(killer: TBaseMob);
var packet: TSendMobDeadPacket;
begin
  if(self.IsPlayer and killer.IsPlayer) then
  begin
    if not(Players[ClientId].Character.PlayerKill) then //0 = false
    begin
      if(Players[ClientId].Character.CP > -25)
        and (Players[ClientId].Character.CP < 50) then
        dec(Players[killer.ClientId].Character.CP, 2)
      else
      if(Players[ClientId].Character.CP >= 50) then
        dec(Players[killer.ClientId].Character.CP, 3)
      else
        dec(Players[killer.ClientId].Character.CP, 1);
    end;
  end;

  MobGrid[CurrentPosition.Y][CurrentPosition.X] := 0;

//  EnemyList.Clear;
//  killer.EnemyList.Remove(ClientId);

  packet.Header.Size := sizeof(TSendMobDeadPacket);
  packet.Header.Code := $338;
  packet.Header.Index := $7530;

  packet.Hold := killer.Character.Hold;
  packet.Killer := killer.ClientId;
  packet.Killed := ClientId;

  killer.AddExp(Character.Exp);
  packet.Exp := killer.Character.Exp;
  Character.CurrentScore.CurHP := 0; // Provavelmente j� est� zerado, mas vamos garantir
  if not(self.IsPlayer) then
  begin
    NPCs[ClientId].TimeKill := Now;
  end;

  SendToVisible(@packet, packet.Header.Size, true);
  SendRemoveMob(DELETE_DEAD);
end;

procedure TBaseMob.AddAffect(affect: TAffect);
var affectSlot: Byte;
  emptySlot: Int8;
begin
  emptySlot := -1;
  for affectSlot := 0 to 15 do
  begin
    if(Character.Affects[affectSlot].Index = affect.Index) then
    begin
      Character.Affects[affectSlot] := affect;
      exit;
    end
    else if (emptySlot = -1) AND (Character.Affects[affectSlot].Index = 0) then
    begin
      emptySlot := affectSlot;
    end;
  end;
  Character.Affects[emptySlot] := affect;
end;

procedure TBaseMob.AddExp(exp: WORD);
var levels : WORD;
begin
  if not(IsPlayer) then
    exit;

  if(Character.Exp + exp > 4100000000) then
    Character.Exp := 4100000000
  else
    Inc(Character.Exp, exp);

  levels := 0;
  while(Character.Exp >= exp_Mortal_Arch[Character.BaseScore.Level + levels]) do
  begin
    Inc(levels);
  end;

  if(levels = 0) then
    exit;

  Inc(Character.BaseScore.Level, levels);

  Inc(Character.pStatus, 5 * levels);
  Inc(Character.pSkill, 3 * levels);
  Inc(Character.pMaster, 3 * levels);

  SendScore;
  SendCurrentHPMP;

  if(IsPlayer) then
  begin
    Players[ClientId].SendClientMessage('+ + +   Subiu de n�vel   + + +');
    Players[ClientId].SendEtc;
    Players[ClientId].SendEmotion(14, 1);
    Players[ClientId].SendEmotion(100, 0);
  end;
end;

procedure TBaseMob.AddToVisible(var mob: TBaseMob);
begin
  if(self.IsPlayer) then
  begin
    if not(VisibleMobs.Contains(mob.ClientId)) then
    begin
      VisibleMobs.Add(mob.ClientId);
      mob.AddToVisible(self);
      mob.SendCreateMob(SPAWN_NORMAL, self.ClientId);
    end;
  end
  else
    if(mob.IsPlayer) then
    begin
      VisibleMobs.Add(mob.ClientId);
      if not(mob.VisibleMobs.Contains(self.ClientId)) then
        mob.VisibleMobs.Add(self.ClientId);
    end;
end;

procedure TBaseMob.RemoveFromVisible(mob : TBaseMob);
begin
  VisibleMobs.Remove(mob.ClientId);
  if(self.IsPlayer) then
    mob.SendRemoveMob(0, self.ClientId);

  if(mob.VisibleMobs.Contains(self.ClientId)) then
    mob.RemoveFromVisible(self);

  if (Target <> NIL) AND (Target.ClientId = mob.ClientId) then
    Target := NIL;
end;

procedure TBaseMob.RemoveItem(slot, slotType: BYTE);
var item: PItem;
begin
  if(slot < 0) then exit;
  TItemFunctions.GetItem(item, self, slot, slotType);
  if(item <> nil) then
    ZeroMemory(item, sizeof(TItem));
end;

procedure TBaseMob.UpdateVisibleList;
var mob : TBaseMob;
  i: WORD;
begin
  IsDirty := false;
  if(VisibleMobs.Count > 0) then // Talvez possamos remover essa verifica��o
  begin
    for i in VisibleMobs do
    begin
      if(GetMob(i, mob) = false) then
      begin
        VisibleMobs.Remove(i);
        continue;
      end;

      if not(CurrentPosition.InRange(mob.CurrentPosition, DISTANCE_TO_FORGET)) then
        RemoveFromVisible(mob);
    end;
  end;

  self.ForEachInRange(DISTANCE_TO_WATCH, procedure(p: TPosition; self, m: TBaseMob)
  begin
    if self.VisibleMobs.Contains(m.ClientId) then
      exit;
    self.AddToVisible(m);
  end);
end;

procedure TBaseMob.UsedSkill(skillId: Byte);
begin
  if _cooldown.ContainsKey(skillId) then
    _cooldown[skillId] := Now
  else
    _cooldown.Add(skillId, Now);
end;

procedure TBaseMob.SendAffects;
var packet : TSendAffectsPacket;
  i : Integer;
begin
  ZeroMemory(@packet, sizeof(TSendAffectsPacket));

  packet.Header.Code := $3B9;
	packet.Header.Size := sizeof(TSendAffectsPacket);
	packet.Header.Index := ClientId;
	for i := 0 to 15 do
	begin
    if(Character.Affects[i].Time >= 0) and (Character.Affects[i].Index <> 0) then
      Move(Character.Affects[i], packet.Affects[i], sizeof(TAffect));
	end;
	SendPacket(@packet, packet.Header.Size);
end;

procedure TBaseMob.SendChat(str: AnsiString);
var packet: TChatPacket;
begin
  ZeroMemory(@packet, sizeof(TChatPacket));
  packet.Header.Size := sizeof(TChatPacket);
  packet.Header.Code := $333;
  packet.Header.Index := ClientId;
  SetString(packet.Chat, PAnsiChar(str), 95);
  SendToVisible(@packet, packet.Header.Size, true);
end;

procedure TBaseMob.SendEquipItems(sendSelf : Boolean = True);
var packet: TRefreshEquips; x: BYTE; sItem: TItem;
effValue : BYTE;
begin

  packet.Header.Size  := sizeof(TRefreshEquips);
  packet.Header.Code  := $36B;
  packet.Header.Index := self.ClientId;

  for x := 0 to 15 do
  begin
    Move(Character.Equip[x],sItem,8);

    if(x = 14)then
        if(sItem.Index >= 2360) and (sItem.Index <= 2389)then
            if(sItem.Effects[0].Index = 0)then
                sItem.Index := 0;

    effValue := TItemFunctions.GetSanc(sItem);
    packet.itemIDEF[x] := sItem.Index;
    packet.Sanc[x]     := effValue;

    packet.pAnctCode[x] := TItemFunctions.GetAnctCode(sItem);
  end;

  SendToVisible(@packet, packet.Header.Size, true);
end;

procedure TBaseMob.SendCreateMob(spawnType : WORD = SPAWN_NORMAL; sendTo : WORD = 0);
var player : TPlayer;
    packet : TSendCreateMobPacket;
    tradePacket : TSendCreateMobTradePacket;
begin
  if(TPlayer.GetPlayer(ClientId, player)) AND (player.Character.IsStoreOpened) then
  begin
    player.GetCreateMobTrade(tradePacket);
    tradePacket.spawnType := spawnType;
    if(sendTo > 0) then
      Server.SendPacketTo(sendTo, @tradePacket, tradePacket.Header.Size)
    else
      SendToVisible(@tradePacket, tradePacket.Header.Size, false);
    exit;
  end
  else if Character.CurrentScore.CurHp <= 0 then
    exit;

  GetCreateMob(packet);
  packet.SpawnType := spawnType;
  if(sendTo > 0) then
    Server.SendPacketTo(sendTo, @packet, packet.Header.Size)
  else
    SendToVisible(@packet, packet.Header.Size);
end;

procedure TBaseMob.SendEmotion(effType, effValue: smallint);
var packet: TSendEmotionPacket;
begin
  packet.Header.Size := sizeof(TSendEmotionPacket);
  packet.Header.Code := $36A;
  packet.Header.Index := ClientId;

  packet.effType := effType;
  packet.effValue := effValue;
  packet.Unknown1 := 0;

  SendToVisible(@packet, packet.Header.Size);
end;

procedure TBaseMob.SendScore;
var packet : TSendScorePacket;
    i : Byte;
begin
  ZeroMemory(@packet, sizeof(TSendScorePacket));

  packet.Header.Size := sizeof(TSendScorePacket);
  packet.Header.Code := $336;
  packet.Header.Index := ClientId;

  GetCurrentScore;

  packet.Critical := Trunc((Character.Critical/10)*5);
  packet.SaveMana := Character.SaveMana;
  packet.GuildIndex := Character.GuildIndex;
  packet.GuildMemberType := Character.GuildMemberType;
  packet.CurHP := Character.CurrentScore.CurHP;
  packet.CurMP := Character.CurrentScore.CurMP;

  packet.RegenHP := Character.RegenHP;
	packet.RegenMP := Character.RegenMP;
  packet.MagicIncrement := Character.MagicIncrement;

  for i := 0 to 3 do
  begin
    packet.Resist[i] := Character.Resist[i];
  end;
  packet.Score := Character.CurrentScore;

  //Character.Affects[0].Index := 102;
  //Character.Affects[0].Time := 100;

  for i := 0 to 15 do begin
    packet.Affects[i].Index := Character.Affects[i].Index;
    packet.Affects[i].Time := Character.Affects[i].Time;
  end;
  SendToVisible(@packet, packet.Header.Size);
end;

procedure TBaseMob.SendCurrentHPMP;
var packet: TSendCurrentHPMPPacket;
begin
  ZeroMemory(@packet, sizeof(TSendCurrentHPMPPacket));

	packet.Header.Size := sizeof(TSendCurrentHPMPPacket);
	packet.Header.Code := $181;
	packet.Header.Index := ClientId;

  packet.CurHP := Character.CurrentScore.CurHP;
	packet.MaxHP := {Character.CurrentScore.CurHP;//}Character.CurrentScore.MaxHP;
	packet.CurMP := Character.CurrentScore.CurMP;
	packet.MaxMP := {Character.CurrentScore.CurMP;//}Character.CurrentScore.MaxMP;

  SendToVisible(@packet, packet.Header.Size);
end;

procedure TBaseMob.ApplyDamage(attacker: TBaseMob; damage: Integer);
var i: Integer;
  sId: Byte;
  mana: Boolean;
  cur: Integer;
begin
  for i := 0 to 14 do
  begin
    if (Character.Affects[i].Index = FM_CONTROLE_MANA) then
    begin
      mana := true;
      sId := i;
      break;
    end;
  end;

  cur := IfThen(mana, Character.CurrentScore.CurMP, Character.CurrentScore.CurHP);
  Dec(cur, damage);

  if mana then
  begin
    Character.CurrentScore.CurMP := IfThen(cur < 0, 0, cur);
    if(cur <= 0) then
    begin
      ZeroMemory(@Character.Affects[sId], SizeOf(TAffect));
      Character.CurrentScore.CurHP := Character.CurrentScore.CurHP + cur;
    end;
  end
  else
  begin
    cur := IfThen(cur < 0, 0, cur);
    Character.CurrentScore.CurHP := cur;
  end;
  if(Character.CurrentScore.CurHP = 0) then
    SendMobDead(attacker);
end;

procedure TBaseMob.SendDamage(target: TBaseMob; skillId: Byte; damage: Integer = -1);
var packet: TProcessAttackOneMob;
  wRange: integer;
begin
  ZeroMemory(@packet, sizeof(TProcessAttackOneMob));
//  SendAffects;

  packet.Header.Size := sizeof(TProcessAttackOneMob);
  packet.Header.Index := ClientId;
  packet.Header.Code := $39D;

  packet.CurrentMp := Character.CurrentScore.CurMP;
  packet.CurrentExp := Character.Exp;
  packet.Motion := 5;

  packet.AttackerID := ClientId;
  packet.AttackCount := 1;
  packet.AttackerPos := CurrentPosition;

  packet.Target.Index := target.ClientId;
  packet.TargetPos := target.CurrentPosition;

  packet.Hold := Character.Hold;

  if (skillId = -1) AND (Character.Equip[6].Index <> 0) then
	begin
		wRange := TItemFunctions.GetItemAbility(Character.Equip[6], EF_RANGE);
		if (wRange = 2)       then skillId := 153
		else if (wRange > 2)  then skillId := 151;
	end;
  packet.SkillIndex := skillId;
//  packet.ReqMp := IfThen(packet.SkillIndex > -1, SkillsData[skillId].ManaSpent, 0);

  damage := IfThen(damage = -1, GetDamage(target, 1), damage);
  packet.Target.Damage := IfThen(damage <= 0, -3, damage); // MMMJR

  target.ApplyDamage(self, damage);

  SendToVisible(@packet, packet.Header.Size);
  DeterminMoveDirection(packet.TargetPos);
end;

procedure TBaseMob.SendDamage(targets: TList<WORD>; skillId: Byte; damage: Integer);
var packet: TProcessAoEAttack;
  wRange: integer;
  target: TBaseMob;
  targetCount: Byte;
  targetId: WORD;
begin
  ZeroMemory(@packet, sizeof(TProcessAoEAttack));
//  SendAffects;

  packet.Header.Size := sizeof(TProcessAoEAttack);
  packet.Header.Index := ClientId;
  packet.Header.Code := $36C;

  packet.CurrentMp := Character.CurrentScore.CurMP;
  packet.CurrentExp := Character.Exp;
  packet.Motion := 5;

  packet.AttackerID := ClientId;
  packet.AttackCount := 1;
  packet.AttackerPos := CurrentPosition;
  packet.TargetPos := CurrentPosition;

  packet.Hold := Character.Hold;

  if (skillId = -1) AND (Character.Equip[6].Index <> 0) then
	begin
		wRange := TItemFunctions.GetItemAbility(Character.Equip[6], EF_RANGE);
		if (wRange = 2)       then skillId := 153
		else if (wRange > 2)  then	skillId := 151;
	end;
  packet.SkillIndex := skillId;
  packet.ReqMp := IfThen(packet.SkillIndex > -1, SkillsData[skillId].ManaSpent, 0);

  targetCount := 0;
  for targetId in targets do
  begin
    if not(GetMob(targetId, target)) then
      continue;

    packet.Targets[targetCount].Index := targetId;
    damage := IfThen(damage = -1, GetDamage(target, 1), damage);
    packet.Targets[targetCount].Damage := IfThen(damage <= 0, -3, damage); // MMMJR
    target.ApplyDamage(self, damage);
    Inc(targetCount);
  end;

  SendToVisible(@packet, packet.Header.Size);
  DeterminMoveDirection(packet.TargetPos);
end;

function TBaseMob.GetItemAmount(itemId: Integer; inv: array of TItem): TItemAmount;
var slot: Integer;
begin
  ZeroMemory(@Result, sizeof(TItemAmount));
  if(itemId < 0) OR (itemId > ItemList.Count) then
    exit;

  Result.ItemId := itemId;
  for slot := 0 to Length(inv) do
  begin
    if(inv[slot].Index = itemId) then
    begin
      Result.Slots[Result.SlotsCount] := slot;
      Inc(Result.SlotsCount);
      Inc(Result.Amount, TItemFunctions.GetItemAmount(inv[slot]));
    end;
  end;
end;

function TBaseMob.GetMaxAbility(eff: integer): integer;
var MaxAbility,i: integer;
ItemAbility: smallint;
begin
  MaxAbility:=0;
  for i := 0 to 15 do begin
    if(Character.Equip[i].Index = 0) then
      continue;

    ItemAbility:= TItemFunctions.GetItemAbility(Character.Equip[i], eff);
    if(MaxAbility < ItemAbility) then
      MaxAbility := ItemAbility;
  end;
  result:=MaxAbility;
end;

class function TBaseMob.GetMob(index: WORD; out mob: TBaseMob): boolean;
begin
  if(index = 0) OR (index > MAX_SPAWN_ID) then
  begin
    result := false;
    exit;
  end;

  if(index <= MAX_CONNECTIONS) then
    mob := Players[index].Base
  else
    mob := NPCs[index].Base;

  if mob.Character = nil then
    exit;

  result := mob.IsActive;
end;


class function TBaseMob.GetMob(pos: TPosition; out mob: TBaseMob): boolean;
begin
  Result := GetMob(MobGrid[pos.Y][pos.X], mob);
end;

class function TBaseMob.GetMob(index: WORD; out mob: PBaseMob): boolean;
begin
  if(index = 0) then
  begin
    result := false;
    exit;
  end;

  if(index <= MAX_CONNECTIONS) then
    mob := @Players[index].Base
  else
    mob := @NPCs[index].Base;

  result := mob.IsActive;
end;

function TBaseMob.GetMobAbility(eff: integer) : integer;
var LOCAL_1,LOCAL_2,LOCAL_19,LOCAL_20,dam1,dam2,arm1,arm2,unique1:integer;
porc,unique2,LOCAL_28: integer;
LOCAL_18: array[0..15] of integer;
begin
  LOCAL_1:=0;
  if(eff = EF_RANGE) then
  begin
    LOCAL_1 := GetMaxAbility(eff);

    LOCAL_2 := Trunc((Character.Equip[0].Index / 10));
    if(LOCAL_1 < 2) and (LOCAL_2 = 3) then
        if((Character.Learn and $100000) <> 0) then
            LOCAL_1 := 2;

    result:=LOCAL_1;
    exit;
  end;

  for LOCAL_19 := 0 to 15 do
  begin
      LOCAL_18[LOCAL_19] := 0;

      LOCAL_20 := Character.Equip[LOCAL_19].Index;
      if(LOCAL_20 = 0) and (LOCAL_19 <> 7) then
          continue;

      if(LOCAL_19 >= 1) and (LOCAL_19 <= 5) then
          LOCAL_18[LOCAL_19] := ItemList[LOCAL_20].Unique;

      if(eff = EF_DAMAGE) and (LOCAL_19 = 6) then
          continue;

      if(eff = EF_MAGIC) and (LOCAL_19 = 7) then
          continue;

      if(LOCAL_19 = 7) and (eff = EF_DAMAGE) then
      begin
        dam1 := (TItemFunctions.GetItemAbility(Character.Equip[6], EF_DAMAGE) +
                    TItemFunctions.GetItemAbility(Character.Equip[6], EF_DAMAGE2));
        dam2 := (TItemFunctions.GetItemAbility(Character.Equip[7], EF_DAMAGE) +
                    TItemFunctions.GetItemAbility(Character.Equip[7], EF_DAMAGE2));

        arm1 := Character.Equip[6].Index;
        arm2 := Character.Equip[7].Index;

        unique1 := 0;
        if(arm1 > 0) and (arm1 < 6500) then
            unique1 := ItemList[arm1].Unique;

        unique2 := 0;
        if(arm2 > 0) and (arm2 < 6500) then
            unique2 := ItemList[arm2].Unique;

        if(unique1 <> 0) and (unique2 <> 0) then
        begin
          porc := 0;
          if(unique1 = unique2) then
              porc := 30
          else
              porc := 20;

          if(dam1 > dam2) then
              LOCAL_1 := Trunc(((LOCAL_1 + dam1) + ((dam2 * porc) / 100)))
          else
              LOCAL_1 := Trunc(((LOCAL_1 + dam2) + ((dam1 * porc) / 100)));

          continue;
        end;

        if(dam1 > dam2) then
            inc(LOCAL_1,dam1)
        else
            inc(LOCAL_1,dam2);

        continue;
      end;

      LOCAL_28 := TItemFunctions.GetItemAbility(Character.Equip[LOCAL_19], eff);
      if(eff = EF_ATTSPEED) and (LOCAL_28 = 1) then
          LOCAL_28 := 10;

      inc(LOCAL_1,LOCAL_28);
    end;

    if(eff = EF_AC) and (LOCAL_18[1] <> 0) then
        if(LOCAL_18[1] = LOCAL_18[2]) and (LOCAL_18[2] = LOCAL_18[3]) and
           (LOCAL_18[3] = LOCAL_18[4]) and (LOCAL_18[4] = LOCAL_18[5]) then
            LOCAL_1 := Trunc(((LOCAL_1 * 105) / 100));

    result := LOCAL_1;
end;

function TBaseMob.InBattle: boolean;
begin
  Result := IfThen(Target <> nil);
end;

function TBaseMob.IsDead: boolean;
begin
  Result := IfThen(Character.CurrentScore.CurHP <= 0);
end;

function TBaseMob.IsMoving: boolean;
begin
  if _prediction.Destination.IsValid then
    Result := IfThen(_prediction.Destination <> CurrentPosition)
  else Result := false;
end;

function TBaseMob.IsPlayer: boolean;
begin
  Result := IfThen(ClientId <= MAX_CONNECTIONS);
end;

procedure TBaseMob.GetCreateMob(out packet : TSendCreateMobPacket);
var i : Byte;
    item : TItem;
begin
  ZeroMemory(@packet, sizeof(TSendCreateMobPacket));

  packet.Header.Size := sizeof(TSendCreateMobPacket);
  packet.Header.Code := $364;
  packet.Header.Index := $7530;

  Move(Character.Name, packet.Name[0], 12);

  packet.Status := Character.CurrentScore;
  packet.ClientId := Clientid;

  packet.Position := CurrentPosition;//Character.Last.X;

  packet.ChaosPoint := 0;//GetGuilty(Clientid);
  packet.CurrentKill := 0;//GetCurKill(Clientid);
  packet.TotalKill := 0;//GetTotKill(Clientid);

  packet.GuildIndex := Character.GuildIndex;

  for i := 0 to 15 do begin
    packet.Affect[i].Time  := (Character.Affects[i].Time) shr 8;
    packet.Affect[i].Index := Character.Affects[i].Index;
  end;

  for i := 0 to 15 do begin
    //effvalue:=0;
    item := Character.Equip[i];
    if(i = 14) then
    begin
      if(item.Index >= 2360) and (item.Index <= 2389) then
      begin
        if(item.Effects[0].Index = 0) then
          item.Index := 0;
      end;
    end;
    //effvalue := GetItemSanc(pitem);
    packet.ItemEff[i] := item.Index;
    packet.AnctCode[i] := TItemFunctions.GetAnctCode(item);
  end;

  packet.SpawnType := 0;
  Move(Character.Tab, packet.Tab, 26);
end;

function TBaseMob.GetCurrentHP(): Integer;
var hp_inc,hp_perc: integer;
begin
  hp_inc := GetMobAbility(EF_HP);
  hp_perc := GetMobAbility(EF_HPADD);

  inc(hp_inc, InitialCharacters[Character.ClassInfo].BaseScore.MaxHP);
  inc(hp_inc, (HPIncrementPerLevel[Character.ClassInfo] * Character.BaseScore.Level));
  inc(hp_inc, (Character.CurrentScore.CON shl 1));
  inc(hp_inc, Trunc(((hp_inc * hp_perc) / 100)));

  if(hp_inc > 64000) then //32
      hp_inc := 64000 //32
  else if(hp_inc <= 0) then
      hp_inc := 1;

  result:=hp_inc;
end;

function TBaseMob.GetCurrentMP(): Integer;
var mp_inc,mp_perc: integer;
begin
  mp_inc := GetMobAbility(EF_MP);
  mp_perc := GetMobAbility(EF_MPADD);

  inc(mp_inc, InitialCharacters[Character.ClassInfo].BaseScore.MaxMP);
  inc(mp_inc, (HPIncrementPerLevel[Character.ClassInfo] * Character.BaseScore.Level));
  inc(mp_inc, (Character.CurrentScore.INT shl 1));
  inc(mp_inc, Trunc(((mp_inc * mp_perc) / 100)));

  if(mp_inc > 64000) then //32
      mp_inc := 64000 //32
  else if(mp_inc <= 0) then
      mp_inc := 1;

  result:=mp_inc;
end;

procedure TBaseMob.GetCurrentScore;
var special: array[0..3] of smallInt;
  special_all,resist,magic,atk_inc,def_inc,i,critical,body: integer;
  evasion: WORD;
  moveSpeed: Byte;
begin
  special_all := GetMobAbility(EF_SPECIALALL);

  special[0] := Character.BaseScore.wMaster + GetMobAbility(EF_SPECIAL1);
  special[1] := Character.BaseScore.fMaster + GetMobAbility(EF_SPECIAL2);
  special[2] := Character.BaseScore.sMaster + GetMobAbility(EF_SPECIAL3);
  special[3] := Character.BaseScore.tMaster + GetMobAbility(EF_SPECIAL4);


  resist := 0;
  if(TItemFunctions.GetSanc(Character.Equip[1]) >= 9) then
    resist := 30;

  Character.Resist[0] := GetMobAbility(EF_RESIST1) + resist;
  Character.Resist[1] := GetMobAbility(EF_RESIST2) + resist;
  Character.Resist[2] := GetMobAbility(EF_RESIST3) + resist;
  Character.Resist[3] := GetMobAbility(EF_RESIST4) + resist;

  for i := 0 to 3 do
  begin
    Inc(special[i], special_all);
    special[i] := IfThen(special[i] > 255, 255, special[i]);
    Character.Resist[i] := IfThen(Character.Resist[i] > 100, 100, Character.Resist[i]);
  end;

  Character.CurrentScore.wMaster := special[0];
  Character.CurrentScore.fMaster := special[1];
  Character.CurrentScore.sMaster := special[2];
  Character.CurrentScore.tMaster := special[3];

  magic := (GetMobAbility(EF_MAGIC) shr 1);
  Character.MagicIncrement := IfThen(magic > 255, 255, magic);

  critical := (GetMobAbility(EF_CRITICAL) div 10) * 5;
  Character.Critical := IfThen(critical > 255, 255, critical);

  Character.RegenHP := GetMobAbility(EF_REGENHP);
  Character.RegenMP := GetMobAbility(EF_REGENMP);
  Character.SaveMana := GetMobAbility(EF_SAVEMANA);

  Character.CurrentScore.STR := Character.BaseScore.STR + GetMobAbility(EF_STR);
  Character.CurrentScore.INT := Character.BaseScore.INT + GetMobAbility(EF_INT);
  Character.CurrentScore.DEX := Character.BaseScore.DEX + GetMobAbility(EF_DEX);
  Character.CurrentScore.CON := Character.BaseScore.CON + GetMobAbility(EF_CON);


  evasion := Character.CurrentScore.DEX div 60;
  Character.AffectInfo.Evasion := IfThen(evasion > 15, 15, evasion);
  Character.CurrentScore.MoveSpeed := Character.BaseScore.MoveSpeed;
  Character.CurrentScore.Level := Character.BaseScore.Level;
  Character.CurrentScore._MerchDir := Character.BaseScore._MerchDir;
//  Character.CurrentScore.Merchant := Character.BaseScore.Merchant;
//  Character.Status.Direction := Character.bStatus.Direction;

  Character.BaseScore.ChaosRate := Character.CurrentScore.ChaosRate;

  moveSpeed := Character.BaseScore.MoveSpeed + GetMaxAbility(EF_RUNSPEED) + Character.AffectInfo.SlowMov;
  Character.AffectInfo.SpeedMov := 0;

  if(Character.Equip[5].Index > 0) then
    Inc(moveSpeed, 2);

  Character.CurrentScore.MoveSpeed := moveSpeed + 1;

  Character.CurrentScore.maxHP := GetCurrentHP; //+ Character.bStatus.MaxHP;
  Character.CurrentScore.maxMP := GetCurrentMP; //+ Character.bStatus.MaxMP;

  if(Character.CurrentScore.MaxHP < Character.CurrentScore.CurHP) then
      Character.CurrentScore.CurHP := Character.CurrentScore.MaxHP;

  if(Character.CurrentScore.MaxMP < Character.CurrentScore.CurMP) then
      Character.CurrentScore.CurMP := Character.CurrentScore.MaxMP;

  AttackSpeed := 100 + (Character.CurrentScore.DEX div 5) + GetMobAbility(EF_ATTSPEED);
  AttackSpeed := IfThen(AttackSpeed > 300, 300, AttackSpeed);

  atk_inc := Character.BaseScore.Attack;
  inc(atk_inc, GetMobAbility(EF_DAMAGE));
  inc(atk_inc, GetMobAbility(EF_DAMAGEADD));
  inc(atk_inc, Trunc(((Character.CurrentScore.STR / 5) * 2)));
  inc(atk_inc, Character.CurrentScore.wMaster);
  inc(atk_inc, Character.CurrentScore.Level);
  Character.CurrentScore.Attack := atk_inc;

  def_inc := Character.BaseScore.Defense;
  inc(def_inc,GetMobAbility(EF_AC));
  inc(def_inc,GetMobAbility(EF_ACADD));
  inc(def_inc,Trunc((Character.CurrentScore.Level * 2)));
  Character.CurrentScore.Defense := def_inc;

  body := Character.Equip[0].Index;
  body := IfThen(body = 32, 21, body);
  Character.ClassInfo := body div 10;


  // Not Implemented
  {
    Character.CapeInfo := 0;
    Character.GuildIndex := 0;
    Character.AffectInfo := 0;
    Character.GuildMemberType := 0;
  }
  GetAffectScore;
end;

procedure TBaseMob.GetAffectScore;
begin
  BuffsData.GetAffectScore(Character);
end;

function TBaseMob.GetDamage(target: TBaseMob; master: Byte): smallint;
var resultDamage: Int16;
  masterFactory, randFactory: Integer;
begin
  ResultDamage := Character.CurrentScore.Attack - (target.Character.CurrentScore.Defense shr 1);
  master := master shr 1;
  master := IfThen(master > 7, 7, master);

  masterFactory := 12 - master;
  if(masterFactory <= 0) then
    masterFactory := 2;
//	masterFactory := IfThen(masterFactory <= 0, 2);

	randFactory := (TFunctions.Rand mod masterFactory) + master + 99;

	ResultDamage := (ResultDamage * RandFactory) div 100;
	if (ResultDamage < -50) then
		ResultDamage := 0
	else if (ResultDamage >= -50) AND (ResultDamage < 0) then
		ResultDamage := (ResultDamage + 50) div 7
	else if (ResultDamage >= 0) AND (ResultDamage <= 50) then
		ResultDamage := ((ResultDamage * 5) shr 2) + 7;

  ResultDamage := IfThen(ResultDamage <= 0, 1, ResultDamage);
	Result := ResultDamage;
end;

function TBaseMob.GetEmptySlot: Byte;
var i: BYTE;
begin
  for i := 0 to 63 do
  begin
    if(Character.Inventory[i].Index = 0) then
    begin
      result := i;
      exit;
    end;
  end;
  result := 254;
end;

procedure TBaseMob.GenerateBabyMob;
//var pos: TPosition; i, j: BYTE; mIndex, id: WORD;
//    party : PParty;
var babyId, babyClientId: WORD;
  party : PParty;
  i, j: Byte;
  pos: TPosition;
begin
  if Character.Equip[14].Index = 0 then
    exit;

  babyId := Character.Equip[14].Index - 2330 + 8;
  if (MobBabyList[babyId].Equip[0].Index = 0) then
    exit;

  babyClientId := TFunctions.GetFreeMob;
  if(babyClientId = 0) then
    exit;

  pos := Character.Last;
  if(not TFunctions.GetEmptyMobGrid(babyClientId, pos.X, pos.Y)) then
    exit;

  NPCs[babyClientId].Create(MobBabyList[babyId], 0, babyClientId, ClientId);
  NPCs[babyClientId].Behaviour := PlayerService;
  NPCs[babyClientId].Character.CurrentScore.Level := Character.CurrentScore.Level;
  NPCs[babyClientId].Character.CurrentScore.CurHP := 100;

  party := @Parties[PartyId];

  if(PartyId = 0) then // N�o est� em grupo
  begin
    party.Leader     := ClientId;
    party.Members[0] := babyClientId;
    SendParty(ClientId, ClientId);
    SendParty(256, babyClientId);
  end
  else
  begin
    i := TFunctions.FindInParty(ClientId,0);
    if (i = 11) then
    begin
      Players[ClientId].SendClientMessage('O grupo est� cheio.');
      exit;
    end;
    //enviar bixo para o grupo q em q o player est�
    NPCs[babyClientId].MobLeaderId := ClientId;
    party.Members[i] := babyClientId;
    Players[party.Leader].Base.SendParty(256, babyClientId);
    for j := 0 to 10 do
      Players[party.Members[j]].Base.SendParty(256, babyClientId);
  end;

  self.Mobbaby := babyClientId;

  NPCs[babyClientId].Character.Last.X := pos.x;
  NPCs[babyClientId].Character.Last.X := pos.y;

  MobGrid[pos.Y][pos.X] := babyClientId;
  NPCs[babyClientId].Base.UpdateVisibleList;
  NPCs[babyClientId].Base.SendCreateMob(SPAWN_BABYGEN);
end;

procedure TBaseMob.UngenerateBabyMob(ungenEffect: WORD);
//var pos: TPosition; i,j: BYTE; party : PParty; find: boolean;
begin
{
  find := false;
  pos.x:= NPCs[Mobbaby].Character.Last.X;
  pos.y:= NPCs[Mobbaby].Character.Last.Y;

  party := @Parties[Parties[NPCs[Mobbaby].MobLeaderId].Leader];
  i := TFunctions.FindInParty(party.Leader, Mobbaby);
  if(i < 11) then
  begin
    Players[party.Leader].SendExitParty(party.Leader, party.Members[i], 0);
    for j := 0 to 10 do
    if(party.Members[j] < 1000) and (party.Members[j] > 0) then
    begin
      find := true;
      Players[party.Members[j]].SendExitParty(party.Leader, party.Members[i], 0);
    end;
  end;

  party.Members[i] := 0;
  NPCs[Mobbaby].Base.SendRemoveMob(DELETE_UNSPAWN);
  ZeroMemory(@NPCs[Mobbaby], sizeof(TNpc));

  MobGrid[pos.y][pos.x]:=0;
  self.Mobbaby:=0;

  if not(find) then
    ZeroMemory(party, sizeof(TParty));
}
end;





procedure TBaseMob.ForEachInRange(range: Byte; proc: TProc<TPosition, TBaseMob, TBaseMob>);
var mobId, index: WORD;
  mob, this: TBaseMob;
  pos: TPosition;
begin
  if not(CurrentPosition.isValid) then
    exit;

  index := self.ClientId;
  this := self;

  CurrentPosition.ForEach(range, procedure(pos: TPosition)
  begin
    mobId := MobGrid[pos.Y][pos.X];
    if(mobId = 0) OR (mobId = index) then
      exit;

    if(mobId <= MAX_CONNECTIONS) then
      mob := Players[mobId].Base
    else
      mob := NPCs[mobId].Base;

    if not(mob.IsActive) then
      exit;

    proc(pos, this, mob);
  end);
end;

class procedure TBaseMob.ForEachInRange(pos: TPosition; range: Byte; proc: TProc<TPosition, TBaseMob>);
var mobId: WORD;
  mob: TBaseMob;
begin
  if not(pos.isValid) then
    exit;

  pos.ForEach(range, procedure(p: TPosition)
  begin
    mobId := MobGrid[pos.Y][pos.X];
    if(mobId = 0) then
      exit;

    if(mobId <= MAX_CONNECTIONS) then
      mob := Players[mobId].Base
    else
      mob := NPCs[mobId].Base;

    if not(mob.IsActive) then
      exit;

    proc(p, mob);
  end);
end;

procedure TBaseMob.ForEachVisible(proc: TProc<TBaseMob>);
var mobId: Integer;
  mob: TBaseMob;
begin
  for mobId in VisibleMobs do
  begin
    if TBaseMob.GetMob(mobId, mob) then
      proc(mob);
  end;
end;

procedure TBaseMob.SetAffect(affectId: Byte; affect: TAffect);
var i: Byte;
begin
  for i := 0 to 15 do
  begin
    if Character.Affects[i].Index = affectId then
    begin
      ZeroMemory(@Character.Affects[i], sizeof(TAffect));
      SendScore;
      if IsPlayer then
        Players[ClientId].SendEtc;
    end;
  end;
end;

procedure TBaseMob.SetDestination(const destination: TPosition);
var dirVector: TPosition;
  speed: byte;
begin
  _prediction.Source := _currentPosition;
  if(_prediction.Source = destination) then
    exit;

  _prediction.Timer.Stop;
  _prediction.Timer.Reset;
  _prediction.Timer.Start; //:= Now;
  _prediction.Destination := destination;
  _prediction.CalcETA(Character.CurrentScore.MoveSpeed);
end;

function TBaseMob.CurrentPosition: TPosition;
var delta: Single;
  id: WORD;
begin
  if not _currentPosition.IsValid then
    _currentPosition := Character.Last;

  if not(_prediction.CanPredict) then
  begin
    Result := _currentPosition;
    exit;
  end;
  Result := _prediction.Interpolate(delta);

//  if not TFunctions.UpdateWorld(ClientId, Result, WORLD_MOB) then
//  begin
//    Result := _currentPosition;
//    exit;
//  end;
//  if Character.Last.Distance(_currentPosition) > 4 then
//    IsDirty := true;

  Character.Last := _currentPosition;
  _currentPosition := Result;
end;

procedure TBaseMob.DeterminMoveDirection(const pos: TPosition);
var moveVector: TPosition;
  dir: BYTE;
begin
  dir := 0;
  moveVector := CurrentPosition - pos;

  if(moveVector.X = 0) AND (moveVector.Y = 0) then
    dir := 4
  else if(moveVector.X < 0) AND (moveVector.Y = 0) then
    dir := 0
  else if(moveVector.X = 0) AND (moveVector.Y < 0) then
    dir := 2
  else if(moveVector.X > 0) AND (moveVector.Y = 0) then
    dir := 6
  else if(moveVector.X = 0) AND (moveVector.Y > 0) then
    dir := 8

  else if(moveVector.X < 0) AND (moveVector.Y < 0) then
    dir := 1
  else if(moveVector.X > 0) AND (moveVector.Y < 0) then
    dir := 3
  else if(moveVector.X < 0) AND (moveVector.Y > 0) then
    dir := 7
  else if(moveVector.X > 0) AND (moveVector.Y > 0) then
    dir := 9;

  Character.BaseScore.Direction := dir;
  Character.CurrentScore.Direction := dir;
end;


procedure TBaseMob.SendMovement(destination: TPosition; calcDirection: Boolean = true);
begin
  SendMovement(destination.X, destination.Y, calcDirection);
end;

procedure TBaseMob.SendMovement(destX, destY : SmallInt; calcDirection: Boolean = true);
var packet: TMovementPacket;
  distance, dir: BYTE;
begin
  packet.Destination.X := destX;
  packet.Destination.Y := destY;

  if not TFunctions.UpdateWorld(ClientId, packet.Destination, WORLD_MOB) then
  begin
    exit;
  end;

  Character.Last := CurrentPosition;
  SetDestination(packet.Destination);
  packet := TFunctions.GetAction(self, packet.Destination, MOVE_NORMAL);
  SendToVisible(@packet, packet.Header.Size, true);
end;

function TBaseMob.Teleport(x, y: SmallInt) : Boolean;
var packet: TMovementPacket;
  src, dest: TPosition;
begin
	packet.Destination.X := x;
	packet.Destination.Y := y;

  Result := TFunctions.UpdateWorld(ClientId, packet.Destination, WORLD_MOB);
  if not Result then
    exit;

  packet := TFunctions.GetAction(self, packet.Destination, MOVE_TELEPORT);
  src := packet.Source;
  dest := packet.Destination;
  SendToVisible(@packet, packet.Header.Size, true);
  //SendRemoveMob(SPAWN_TELEPORT);

  Character.Last := src;
  _currentPosition := dest;

  UpdateVisibleList();
  SendCreateMob(SPAWN_TELEPORT);
end;

function TBaseMob.Teleport(position: TPosition) : Boolean;
begin
  Result := Teleport(position.X, position.Y);
end;

{ TPrediction }
procedure TPrediction.Create;
begin
  Timer := TStopwatch.Create;
end;

function TPrediction.Delta: Single;
begin
  if ETA > 0 then
    Result := Elapsed / ETA
  else
    Result := 1;
end;

function TPrediction.Elapsed: Integer;
begin
  Result := Timer.ElapsedTicks;
end;

function TPrediction.CanPredict: Boolean;
begin
  Result := ((ETA > 0) AND (Source.IsValid) AND (Destination.IsValid));
end;


function TPrediction.Interpolate(out d: Single): TPosition;
begin
  d := Delta;
  if(d >= 1) then
  begin
    ETA := 0;
    Result := Destination;
  end
  else Result := TPosition.Lerp(Source, Destination, d);
end;

procedure TPrediction.CalcETA(speed: Byte);
var dist: WORD;
begin
  dist := Source.Distance(Destination);
	speed := speed * 190;
	ETA := (AI_DELAY_MOVIMENTO + (dist * (1000 - speed)));

//  ETA := dist / IfThen(speed = 0, 1, speed);
//  ETA := IfThen(ETA = 0, 0.01, ETA);
end;

end.
