unit Player;

interface

uses Windows, ClientConnection, WinSock2, PlayerData, MiscData, BaseMob,
      SysUtils, Generics.Collections, Packets, Log, System.Threading;

type PPlayer = ^TPlayer;
TPlayer = record
  procedure Create(clientId : WORD; conn: TClientConnection);
  procedure Destroy;

  private
    SelectedCharacterIndex : integer;

  public
    Base : TBaseMob;
    Status : TPlayerStatus;
    SubStatus : TPlayerStatus;
    Character : TPlayerCharacter;
    Account : TAccountFile;
    CountTime : TDateTime;
    Connection : TClientConnection;
    KnowInitItems: TList<integer>;


    procedure UpdateVisibleDropList();

    procedure SendPacket(packet : pointer; size : WORD; encrypt: Boolean = true);
    procedure SendSignal(headerClientId, packetCode: WORD); overload;
    procedure SendSignal(headerClientId, packetCode: WORD; optional: array of Integer); overload;
    function LoadAccount(userName : string) : Boolean;
    procedure SaveAccount();
    function BackToCharList() : Boolean;

    procedure GetCreateMobTrade(out packet : TSendCreateMobTradePacket);

    // Sends
    procedure RefreshMoney();
    procedure SendClientMessage(str: AnsiString);
    procedure RefreshInventory();
    procedure SendCharList(code : WORD = $10E);
    function SendToWorld(charId : Byte) : Boolean;
    procedure SendWeather();
    procedure SendCreateItem(invType : smallint; invSlot : smallint; item: TItem);
    procedure SendCreateDropItem(initId: WORD);
    procedure SendDeleteDropItem(initId: WORD; sendSelf: Boolean = False);
    procedure SendDeleteItem(invType : smallint; invSlot : smallint);
    procedure SendEtc;
    procedure SendEmotion(effType, effValue: integer);

    procedure CloseTrade;

    procedure SendAutoTrade(player : TPlayer);
    //procedure SendParty(leader, member : WORD);
    procedure SendExitParty(leader, member, exitid: WORD);

    function ReceiveData() : Boolean;

    class function GetPlayer(index: WORD; out player: TPlayer): boolean; static;
    class procedure ForEach(proc: TProc<PPlayer>); overload; static;
    class procedure ForEach(proc: TProc<PPlayer, TParallel.TLoopState>); overload; static;
end;


implementation
uses GlobalDefs, Functions, ItemFunctions;

{ TPlayer }
procedure TPlayer.Create(clientId : WORD; conn: TClientConnection);
begin
  ZeroMemory(@self, sizeof(TPlayer));
  Base.Create(@self.Character.Base, clientId);

  self.Connection := conn;
  KnowInitItems := TList<integer>.Create;
end;

procedure TPlayer.Destroy;
begin
  Account.Header.IsActive := false;
  SaveAccount;
  Connection.Destroy;
  Base.Destroy;
end;

procedure TPlayer.UpdateVisibleDropList();
var initId : WORD;
    x, y : Integer;
    i: WORD;
    initItem: TInitItem;
begin
  if(KnowInitItems.Count > 0) then // Talvez possamos remover essa verifica��o
  begin
    for initId in KnowInitItems do
    begin
      initItem := InitItems[initId];
      if not(Base.Character.Last.InRange(initItem.Pos, DISTANCE_TO_FORGET)) then
        KnowInitItems.Remove(initId);
    end;
  end;

  for x := Base.Character.Last.X - DISTANCE_TO_WATCH to Base.Character.Last.X + DISTANCE_TO_WATCH do
  begin
    for y := Base.Character.Last.Y - DISTANCE_TO_WATCH to Base.Character.Last.Y + DISTANCE_TO_WATCH do
    begin
      if (x > 4096) or (x < 0) or (y > 4096) or (y < 0) then
        continue;

      initId := ItemGrid[y][x];
      if(initId = 0) or (KnowInitItems.Contains(initId)) then
        continue;

      KnowInitItems.Add(initId);
      SendCreateDropItem(initId);
    end;
  end;
end;

function TPlayer.BackToCharList : Boolean;
begin
  MobGrid[Character.Base.Last.Y][Character.Base.Last.X] := 0;
  ZeroMemory(@Character, sizeof(TPlayerCharacter));
  Status := CharList;
  SelectedCharacterIndex := -1;
  SendSignal(self.Base.ClientId, $116);
  Result := true;
end;

procedure TPlayer.CloseTrade;
var i: BYTE;
begin
  ZeroMemory(@self.Character.Trade, sizeof(TTrade));
  for i := 0 to 14 do
    self.Character.Trade.TradeItemSlot[i] := -1;
  self.SendSignal(Base.ClientId, $384);
end;

class procedure TPlayer.ForEach(proc: TProc<PPlayer, TParallel.TLoopState>);
begin
  TParallel.For(1, InstantiatedPlayers, procedure(i : Integer; state : TParallel.TLoopState)
  var player: PPlayer;
  begin
    player := @Players[i];
    if(player = nil) OR not(player.Base.IsActive) then
      exit;
    proc(player, state);
  end);
end;

class procedure TPlayer.ForEach(proc: TProc<PPlayer>);
var i: Integer;
  player: PPlayer;
begin
  for i := 1 to InstantiatedPlayers do
  begin
    player := @Players[i];
    if(player = nil) OR not(player.Base.IsActive) then
      continue;
    proc(player);
  end;
end;

procedure TPlayer.GetCreateMobTrade(out packet: TSendCreateMobTradePacket);
var pak : TSendCreateMobPacket;
begin
  Base.GetCreateMob(pak);
  ZeroMemory(@packet, sizeof(TSendCreateMobTradePacket));
  Move(pak, packet, 132);

  packet.Header.Code  := $363;
  packet.Header.Size  := sizeof(TSendCreateMobTradePacket);
  packet.Header.Index := $7530;
  packet.spawnType    := $CC;
  packet.MemberType   := $CC;

  packet.StoreName    := Character.TradeStore.Name;
  packet.StoreName[0] := Character.TradeStore.Name[0];
  //Move(Player[index].TradeLoja.Name,packet.StoreName[0],24);
  //Player[index].TradeLoja.Name
  packet.x2       := packet.Position.X;
  packet.y2       := packet.Position.Y;
  packet.clientId := packet.Index;
  packet.Clock    := TFunctions.Clock;

  FillChar(packet.unk[0],13,$CC);
  FillChar(packet.unk3[0],8,$CC);
  FillChar(packet.unk4[0],4,$CC);
end;


class function TPlayer.GetPlayer(index: WORD; out player: TPlayer): boolean;
begin
  Result := false;
  if(index = 0) OR (index > MAX_CONNECTIONS) then
    exit;

  player := Players[index];
  Result := player.Base.IsActive;
end;

procedure TPlayer.SendPacket(packet : pointer; size : WORD; encrypt: Boolean);
begin
  if(Connection.Socket = -1) then exit;
  Connection.SendPacket(packet, size, encrypt);
end;

procedure TPlayer.SendSignal(headerClientId, packetCode: WORD);
var signal : TPacketHeader;
begin
  ZeroMemory(@signal, sizeof(TPacketHeader));

  signal.Size := 12;
  signal.Index := headerClientId;
  signal.Code := packetCode;

  SendPacket(@signal, signal.Size)
end;

procedure TPlayer.SendSignal(headerClientId, packetCode: WORD; optional: array of Integer);
var signal : TPacketHeader;
  signalBuffer: array of BYTE;
  i: Integer;
  a: Integer;
begin
  ZeroMemory(@signal, sizeof(TPacketHeader));
  signal.Size := 12 + Length(optional) * SizeOf(Integer);
  signal.Index := headerClientId;
  signal.Code := packetCode;

  SetLength(signalBuffer, signal.Size);

  Move(signal, signalBuffer[0], sizeof(TPacketHeader));

  Move(optional[0], signalBuffer[12], Length(optional) * 4);

  SendPacket(@signalBuffer, signal.Size);
end;

procedure TPlayer.SendEmotion(effType, effValue: integer);
var packet: TRequestEmotion;
begin
    packet.Header.Size  := sizeof(TRequestEmotion);
    packet.Header.Code  := $36A;
    packet.Header.Index := Base.ClientId;

    packet.effType  := effType;
    packet.effValue := effValue;
    packet.Unknown1 := 0;

    Base.SendToVisible(@packet, packet.Header.Size, true);
end;

procedure TPlayer.SendCharList(code : WORD);
var packet: TSendToCharListPacket; i : BYTE;
begin
  ZeroMemory(@packet, sizeof(TSendToCharListPacket));

  packet.Header.Code := code;
  packet.Header.Index := 30002;
  for i := 0 to 3 do
  begin
    if(Account.Characters[i].Base.Equip[0].Index = 0) then
      continue;
    Move(Account.Characters[i].Base.Name, packet.CharactersData.Name[i][0], 15);
    Move(Account.Characters[i].Base.Equip, packet.CharactersData.Equip[i][0], sizeof(TItem) * 16);

    if (Account.Characters[i].Base.Equip[0].Index in [22,23,24,25,32]) then
      packet.CharactersData.Equip[i][0].Index := Account.Characters[i].Base.Equip[0].Effects[2].Value;

    packet.CharactersData.Status[i] := Account.Characters[i].Base.CurrentScore;
    packet.CharactersData.GuildIndex[i] := Account.Characters[i].Base.GuildIndex;
    packet.CharactersData.Gold[i] := Account.Characters[i].Base.Gold;
    packet.CharactersData.Exp[i] := Account.Characters[i].Base.Exp;
  end;

  if(Status = CharList) then
  begin
    packet.Header.Size := sizeof(TUpdateCharacterListPacket);
    SendPacket(@packet, packet.Header.Size);
    exit;
  end;

  packet.Gold := Account.Header.StorageGold;
  Move(Account.Header.StorageItens[0], packet.Storage[0], sizeof(TItem) * 127);
  packet.Name := Account.Header.Username;
  packet.Keys := Account.Header.Password;
  Status := CharList;
  SubStatus := Senha2;

  packet.Header.Size := sizeof(TSendToCharListPacket);
  SendPacket(@packet, packet.Header.Size);
end;

function TPlayer.LoadAccount(userName: string): Boolean;
var f : file of TAccountFile;
    local : string;
begin
  ZeroMemory(@Account, sizeof(TAccountFile));
  if(TFunctions.IsLetter(userName) = true) then
    local := CurrentDir + '\ACCS\' + userName[1] + '\' + Trim(userName) + '.acc'
  else
    local := CurrentDir + '\ACCS\etc\' + Trim(userName) + '.acc';

  if not(FileExists(local)) then
    exit;

  try
    AssignFile(f, local);
    Reset(f);
    Read(f, Account);
    CloseFile(f);
    Result := true;
  except
    CloseFile(f);
  end;
end;


function TPlayer.SendToWorld(charId : Byte) : Boolean;
var packet : TSendToWorldPacket;
    spawnPosition : TPosition;
    direction: BYTE;
begin
  Result := false;
  spawnPosition := TFunctions.GetStartXY(self, charId);

  if not(TFunctions.GetEmptyMobGrid(Base.ClientId, spawnPosition.X, spawnPosition.Y)) then
  begin
    SendClientMessage('Falta espa�o no mapa.');
    exit;
  end;

  ZeroMemory(@Character, sizeof(TPlayerCharacter));
  ZeroMemory(@packet, sizeof(TSendToWorldPacket));

  SelectedCharacterIndex := charId;

  packet.Header.Size := sizeof(TSendToWorldPacket);
  packet.Header.Code := $114;
  packet.Header.Index := $7531;

  Move(Account.Characters[charId], Character, sizeof(TCharacterDB));
  {
  Character.Base.Affects[0].Time := 80;
  Character.Base.Affects[0].Index := 37;
  }
  if(Base.IsDead) then
  begin
    Character.Base.CurrentScore.CurHP := Character.Base.CurrentScore.MaxHP;
  end;

  Character.Base.ClientId := Base.ClientId;
  Base.GetCurrentScore;

  Character.Base.Last := spawnPosition;
  direction := Character.Base.CurrentScore.Direction;

  MobGrid[Character.Base.Last.Y][Character.Base.Last.X] := Base.Clientid;

  Move(Character.Base, packet.Character, sizeof(TCharacter));
  packet.Point := spawnPosition;
  {
  FillChar(packet.Character.SkillBar1[0], 4, -1);
  FillChar(packet.Character.SkillBar2[0],16, -1);
  }
  SendPacket(@packet, packet.Header.Size);


  Base.UpdateVisibleList;
  UpdateVisibleDropList;

  Base.SendAffects;
  Base.SendScore;
  SendWeather;

  Base.SendCreateMob(SPAWN_TELEPORT);

  if(Character.Base.Equip[14].Index >= 2330) and (Character.Base.Equip[14].Index <= 2359) then
    Base.GenerateBabyMob;

  Status := Playing;
  SendSignal(0, $3A0);
  Result := true;
end;

procedure TPlayer.SendWeather;
var packet : TSendWeatherPacket;
begin
  packet.Header.Size := sizeof(TSendWeatherPacket);
  packet.Header.Code := $18B;
  packet.Header.Index := Base.ClientId;
  packet.WeatherId := BYTE(CityWeather[BYTE(Character.CurrentCity)].Condition);
  SendPacket(@packet, packet.Header.Size);
end;


procedure TPlayer.SaveAccount;
var
  f : file of TAccountFile;
  local : string;
begin
  if(TFunctions.IsLetter(Account.Header.Username)) then
    local := CurrentDir + '\ACCS\' + Trim(Account.Header.Username[1]) + '\' + Trim(Account.Header.Username) + '.acc'
  else
    local := CurrentDir + '\ACCS\etc\' + Trim(Account.Header.Username)+ '.acc';

  if(Status = TPlayerStatus.Playing) then
  begin
    Move(Character, Account.Characters[SelectedCharacterIndex], sizeof(TCharacterDB));
    //Move(Player[index].Char.Status, Player[index].Char.Status, sizeof(TStatus));
  end;
  AssignFile(f, local);
  ReWrite(f);
  Write(f, Account);
  CloseFile(f);
end;


procedure TPlayer.RefreshMoney;
var packet : TRefreshMoneyPacket;
begin
  ZeroMemory(@packet, sizeof(TRefreshMoneyPacket));

  packet.Header.Size := sizeof(TRefreshMoneyPacket);
	packet.Header.Code := $3AF;
	packet.Header.Index := Base.Clientid;
	packet.Gold := Character.Base.Gold;

  SendPacket(@packet, packet.Header.Size);
end;

procedure TPlayer.SendClientMessage(str: AnsiString);
var packet: TClientMessagePacket;
begin
  ZeroMemory(@packet, sizeof(TClientMessagePacket));

  packet.Header.Size := sizeof(TClientMessagePacket);
  packet.Header.Code := $101;
  packet.Header.Index := 0;

  SetString(packet.Message, PAnsiChar(str), 95);

  SendPacket(@packet, packet.Header.Size);
end;

function TPlayer.ReceiveData() : Boolean;
var ReceivedBytes : integer;
begin
  Result := false;
	ZeroMemory(@Connection.RecvBuffer, 3000);
	ReceivedBytes := Recv(Connection.Socket, Connection.RecvBuffer, 3000, 0);
	if(ReceivedBytes <= 0) then
  begin
    exit;
  end;
  Result := Server.OnReceivePacket(self, Connection.RecvBuffer, ReceivedBytes);
end;

procedure TPlayer.RefreshInventory;
var packet: TRefreshInventoryPacket;
begin
  ZeroMemory(@packet, sizeof(TRefreshInventoryPacket));

  packet.Header.Size := sizeof(TRefreshInventoryPacket);
	packet.Header.Code := $185;
	packet.Header.Index := Base.Clientid;

	packet.Gold := Character.Base.Gold;

  //packet.Inventory := Character.Base.Inventory;
  Move(Character.Base.Inventory[0], packet.Inventory[0], sizeof(TItem) * 64);

  SendPacket(@packet, packet.Header.Size)
end;

procedure TPlayer.SendCreateItem(invType : smallint; invSlot : smallint; item: TItem);
var packet: TSendCreateItemPacket;
begin
  ZeroMemory(@packet, sizeof(TSendCreateItemPacket));

  packet.Header.Size := sizeof(TSendCreateItemPacket);
  packet.Header.Code := $182;
  packet.Header.Index := Base.ClientId;

  packet.invType := invType;
  packet.invSlot := invSlot;

  if(@item = NIL) then
      fillchar(packet.itemData, 0, sizeof(Item))
  else
      Move(item, packet.itemData, sizeof(item));

  SendPacket(@packet, packet.Header.Size);
end;

procedure TPlayer.SendCreateDropItem(initId: WORD);
var packet: TSendCreateDropItem; initItem : TInitItem;
begin
  initItem := InitItems[initId];
  ZeroMemory(@packet, sizeof(TSendCreateDropItem));

  packet.Header.Size := sizeof(TSendCreateDropItem);
  packet.Header.Code := $26E;
  packet.Header.Index := $7530;

  packet.Pos := initItem.Pos;
  packet.initId := initId + 10000;

  Move(initItem.Item, packet.item, sizeof(TItem));

  packet.rotation := 0;
  packet.status   := 0;

  packet.Unknown := 0;

  self.Base.SendToVisible(@packet, packet.Header.Size);
end;

procedure TPlayer.SendDeleteDropItem(initId: WORD; sendSelf: Boolean = False);
var packet: TDropDelete;
begin
  ZeroMemory(@packet, sizeof(TDropDelete));

  packet.Header.Size := sizeof(TDropDelete);
  packet.Header.Code := $16F;
  packet.Header.Index := $7530;

  packet.initId := initId;

  packet.Unknown1 := 0;

  if (sendSelf) then
    self.SendPacket(@packet, packet.Header.Size)
  else
    self.Base.SendToVisible(@packet, packet.Header.Size);
end;

procedure TPlayer.SendDeleteItem(invType : smallint; invSlot : smallint);
var packet: TSendDeleteItemPacket;
begin
  ZeroMemory(@packet, sizeof(TSendDeleteItemPacket));

  packet.Header.Size := sizeof(TSendDeleteItemPacket);
  packet.Header.Code := $175;
  packet.Header.Index := Base.ClientId;

  packet.invType := invType;
  packet.invSlot := invSlot;

  packet.Pos := Character.Base.Last;

  SendPacket(@packet, packet.Header.Size);
end;


procedure TPlayer.SendEtc;
var packet: TRefreshEtcPacket;
begin
  ZeroMemory(@packet, sizeof(TRefreshEtcPacket));

  packet.Header.Size := sizeof(TRefreshEtcPacket);
	packet.Header.Code := $337;
	packet.Header.Index := Base.ClientId;

	packet.hold := Character.Base.Hold;
	packet.exp := Character.Base.Exp;
	packet.learn := Character.Base.Learn;
  packet.Gold:= Character.Base.Gold;
	packet.StatusPoint := Character.Base.pStatus;
	packet.MasterPoint := Character.Base.pMaster;
	packet.SkillsPoint := Character.Base.pSkill;
	packet.MagicIncrement := Character.Base.MagicIncrement;

  SendPacket(@packet, packet.Header.Size);
end;

procedure TPlayer.SendAutoTrade(player : TPlayer);
var packet: TRequestOpenPlayerStorePacket;
begin
  packet.Header.Size  := sizeof(TRequestOpenPlayerStorePacket);
  packet.Header.Code  := $397;
  packet.Header.Index := player.Base.clientId;

  Move(self.Character.TradeStore,packet.Trade,sizeof(TTradeStore));

  player.SendPacket(@packet, packet.Header.Size);
end;

procedure TPlayer.SendExitParty(leader, member, exitid: WORD);
var packet: TExitPartyPacket;
begin
  packet.Header.Size  :=sizeof(TExitPartyPacket);
  packet.Header.Code  :=$37E;
  packet.Header.Index :=member;
  packet.ExitId       :=exitid;
  packet.unk          :=0;

  SendPacket(@packet,packet.Header.Size);
end;

end.

