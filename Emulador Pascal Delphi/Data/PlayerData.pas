unit PlayerData;

interface

uses MiscData, Types, Generics.Collections;


type TCity = (Armia, Azram, Erion, Karden);
type TWeatherCondition = (Normal, Rain, Snow, HeavySnow);
type TCitizenship = (None = -1, Server1, Server2, Server3);
type TClassLevel = (Mortal, Arch, Celestial, SubCelestial, Hardcore);
type TPlayerStatus = (WaitingLogin, CharList, Senha2, Waiting, Playing);

type TInitItem = Record
  Item : TItem;
  ClientId : WORD;
  TimeDrop : TDateTime;
  Pos : TPosition;
End;


type TStatus = Record
  private
    function GetMerchDir(const Index: Integer): Byte;
    procedure SetMerchDir(const Index: Integer; const Value: Byte);
    function GetMoveChaos(const Index: Integer): Byte;
    procedure SetMoveChaos(const Index: Integer; const Value: Byte);

  public
    Level: WORD;
    Defense: WORD;
    Attack: WORD;
    (*
    struct
    {
      BYTE Merchant : 4;
      BYTE Direction : 4;
    } Merchant;
    *)
    _MerchDir: BYTE;
    (*
    struct
    {
      BYTE Speed : 4;
      BYTE ChaosRate : 4;
    } Move;
    *)
    _MoveChaos: BYTE;

    MaxHP, MaxMP: WORD;
    CurHP, CurMP: WORD;

    Str,Int: WORD;
    Dex,Con: WORD;

    wMaster: Byte;
    fMaster: Byte;
    sMaster: Byte;
    tMaster: Byte;

    property Merchant : Byte index 0004 read GetMerchDir write SetMerchDir;
    property Direction : Byte index 0404 read GetMerchDir write SetMerchDir;

    property MoveSpeed : Byte index 0004 read GetMoveChaos write SetMoveChaos;
    property ChaosRate : Byte index 0404 read GetMoveChaos write SetMoveChaos;
end;

type TCharacterListData = Record
  PosX: Smallint;
  PosY: Smallint;
  Name:Array[0..15]of AnsiChar;

  Status: TStatus;
  Equip: array[0..15] of TItem;

  GuildIndex: Word;

  Gold: Integer;
  Exp: Integer;
end;

type TAffect = Record
	Index: BYTE;
	Master: BYTE;
	Value: smallint;
	Time: integer;
end;

type TAffectInfo = record
  private
    function GetBits(const Index: Integer): Byte;
    procedure SetBits(const Index: Integer; const Value: Byte);
  (*
  struct
  {
      BYTE SlowMov : 1;
      BYTE DrainHP : 1;
      BYTE VisionDrop : 1;
      BYTE Evasion : 1;
      BYTE Snoop : 1;
      BYTE SpeedMov : 1;
      BYTE SkillDelay : 1;
      BYTE Resist : 1;
  } AffectInfo;
  *)
  public
    _Info: Byte;

    property SlowMov : Byte index 0001 read GetBits write SetBits;
    property DrainHP : Byte index 0101 read GetBits write SetBits;
    property VisionDrop : Byte index 0201 read GetBits write SetBits;
    property Evasion : Byte index 0301 read GetBits write SetBits;
    property Snoop : Byte index 0401 read GetBits write SetBits;
    property SpeedMov : Byte index 0501 read GetBits write SetBits;
    property SkillDelay : Byte index 0601 read GetBits write SetBits;
    property Resist : Byte index 0701 read GetBits write SetBits;
end;

type PCharacter = ^TCharacter;
TCharacter = record
  private
    function GetBits(const Index: Integer): Byte;
    procedure SetBits(const Index: Integer; const Value: Byte);

  public
    Name: array[0..15] of AnsiChar;
    CapeInfo: BYTE; // Race
    (*
    struct
    {
      UINT8 Merchant : 6;
      UINT8 CityID : 2;
    } Info;
    *)
    _MerchCity: BYTE;

    GuildIndex: WORD;
    ClassInfo: BYTE;


    AffectInfo: TAffectInfo;
    QuestInfo: WORD;

    Gold: Integer;
    Exp: Integer;

    Last: TPosition;
    BaseScore: TStatus;
    CurrentScore: TStatus;

    Equip: array[0..15] of TItem;
    Inventory: array[0..63] of TItem;

    Learn: LongWord;
    pStatus: WORD;
    pMaster: WORD;
    pSkill: WORD;
    Critical: BYTE;
    SaveMana: BYTE;

    SkillBar1: array[0..3] of shortint;
    GuildMemberType: shortint;

    MagicIncrement: BYTE;
    RegenHP: BYTE;
    RegenMP: BYTE;

    Resist: array[0..3] of BYTE;

    SlotIndex: WORD;
    ClientId: WORD; // Nunca usar isso em npcs
    unk1: Smallint;

    SkillBar2: array[0..15] of int8;
    Evasion: Int16;
    Hold: integer;

    Tab: array [0..25] of AnsiChar;
    {
    // Estrtura modificada seguindo o Emulador XTS
    HPDRAIN : Integer;
    Absorcao: DWORD;
    TimeStamp: Integer;
    AtkSpeed: WORD;
    MobType: WORD;
    Destination: TVector2D;
    Zeros: array[0..11] of BYTE;
    Command: array[0..23] of AnsiChar;
    // ----------------------------------
    }
    Affects: array[0..15] of TAffect;
    ClasseMaster: integer;

    function ClassLevel: TClassLevel;

    property Merchant : Byte index 0006 read GetBits write SetBits;
    property CityId : Byte index 0602 read GetBits write SetBits;

    function HaveSkill(SkillId: Byte): Boolean;
end;

type TAccountHeader = Record
  AccountId: Integer;
  Username: String[15];
  Password: String[11];
  IsActive: Boolean;
  StorageGold: Integer;
  StorageItens: array[0..127] of TItem;
  NumericToken: String[5];
  //CharacterIndexes: array[0..3] of Integer;
end;

type PParty = ^TParty;
TParty = Record
  Leader: WORD;
  Members: TList<WORD>;
  RequestId: WORD;

  function AddMember(memberClientId: WORD): Boolean;
End;

type TTradeStore = Record
  Name: string[23];
  Item: array[0..11] of TItem;
  Slot: array[0..11] of BYTE;
  Gold: array[0..11] of integer;
  Unknown, Index: smallint;
end;

type TTrade = Record
	IsTrading: boolean;
  Confirm: boolean;
	Waiting: boolean;

	Gold: integer;

	Timer: TDateTime;

	OtherClientid: WORD;

	Itens: array[0..14] of TItem;
	TradeItemSlot: array[0..14] of shortint;
end;

type TCharacterDB = record
  Index : Integer;  // Id �nico do personagem
  Base : TCharacter;
  LastAction: TTime;
  PlayerKill: Boolean;
  CurrentKill: BYTE;
  TotalKill: WORD;
  CP: integer;
  Fame: WORD;
  CurrentCity: TCity;
  //Citizenship: TCitizenship;
end;

type TPlayerCharacter = record
  Index : Integer;  // Id �nico do personagem
  Base : TCharacter;
  LastAction: TTime;
  PlayerKill: Boolean;
  CurrentKill: BYTE;
  TotalKill: WORD;
  CP: integer;
  Fame: WORD;
  CurrentCity: TCity;
  //Citizenship: TCitizenship;
  //TCharacterDB

  CurrentQuest: Int8;
  QuestEntraceTime: TDateTime;
  TradeStore: TTradeStore;
  IsStoreOpened : Boolean;
  Trade: TTrade;
  Absorcao: Integer;
  AttackSpeed: WORD;
  Mobbaby: WORD;
  Evasion : BYTE;
end;

type TWeather = Record
  Condition : TWeatherCondition;
  Time : TDateTime;
  Next : TDateTime;
End;


type TAccountFile = Record
  Header: TAccountHeader;
  Characters: array[0..3] of TCharacterDB;
end;

type TCharacterFile = Record
  Name: String[15];
end;

type TItemEffect = Record
  Index: Smallint;
  Value: Smallint;
end;

type PNpcGenerator = ^TNpcGenerator;
TNpcGenerator = record
  MinuteGenerate : smallint;
  LeaderName : string[15];
  FollowerName : string[15];
  LeaderCount : BYTE;
  FollowerCount : BYTE;
  RouteType : BYTE;
  SpawnPosition : TPosition;
  SpawnWait : shortint;
  SpawnSay : string[95];
  Destination : TPosition;
  DestSay: string[95];
  DestWait : shortint;
  ReviveTime: Cardinal;
  AttackDelay: Cardinal;
End;

const MAX_MOB_BABY = 38;

const MobBabyNames: array[0..MAX_MOB_BABY - 1] of string =
('Condor', 'Javali', 'Lobo', 'Urso', 'Tigre',
'Gorila', 'Dragao_Negro', 'Succubus', 'Porco',
'Javali_', 'Lobo_', 'Dragao_Menor', 'Urso_',
'Dente_de_Sabre', 'Sem_Sela', 'Fantasma', 'Leve',
'Equipado', 'Andaluz', 'Sem_Sela_', 'Fantasma_',
'Leve_', 'Equipado_', 'Andaluz_', 'Fenrir', 'Dragao',
'Grande_Fenrir', 'Tigre_de_Fogo', 'Dragao_Vermelho',
'Unicornio', 'Pegasus', 'Unisus', 'Grifo', 'Hippo_Grifo',
'Grifo_Sangrento', 'Svadilfari', 'Sleipnir', '');

const exp_Mortal_Arch: array[0..399] of Cardinal = (
500,
1124,
1826,
2610,
3480,
4440,
5494,
6646,
7900,
9260,
10893,
12817,
15050,
17610,
20515,
23783,
27432,
31480,
35945,
40845,
46251,
52187,
58677,
65745,
73415,
81711,
90657,
100277,
110595,
121635,
133647,
146671,
160747,
175915,
192215,
209687,
228371,
248307,
269535,
292095,
316151,
341751,
368943,
397775,
428295,
460551,
494591,
530463,
568215,
607895,
649715,
693731,
739999,
788575,
839515,
892875,
948711,
1007079,
1068035,
1131635,
1198670,
1269230,
1343405,
1421285,
1502960,
1588520,
1678055,
1771655,
1869410,
1971410,
2078255,
2190055,
2306920,
2428960,
2556285,
2689005,
2827230,
2971070,
3120635,
3276035,
3438521,
3608249,
3785375,
3970055,
4162445,
4362701,
4570979,
4787435,
5012225,
5245505,
5488163,
5740379,
6002333,
6274205,
6556175,
6848423,
7151129,
7464473,
7788635,
8123795,
8460174,
8797774,
9136597,
9476645,
9817920,
10160424,
10504159,
10849127,
11195330,
11542770,
11892311,
12243959,
12597720,
12953600,
13311605,
13671741,
14034014,
14398430,
14764995,
15133715,
15508850,
15890450,
16278565,
16673245,
17074540,
17482500,
17897175,
18318615,
18746870,
19181990,
19625811,
20078403,
20539836,
21010180,
21489505,
21977881,
22475378,
22982066,
23498015,
24023295,
24559110,
25105558,
25662737,
26230745,
26809680,
27399640,
28000723,
28613027,
29236650,
29871690,
30517485,
31174125,
31841700,
32520300,
33210015,
33910935,
34623150,
35346750,
36081825,
36828465,
37587867,
38360139,
39145389,
39943725,
40755255,
41580087,
42418329,
43270089,
44135475,
45014595,
45904870,
46806370,
47719165,
48643325,
49578920,
50526020,
51484695,
52455015,
53437050,
54430870,
55439542,
56463162,
57501826,
58555630,
59624670,
60709042,
61808842,
62924166,
64055110,
65201770,
66366010,
67547930,
68747630,
69965210,
71200770,
72454410,
73726230,
75016330,
76324810,
77651770,
78985354,
80325578,
81672458,
83026010,
84386250,
85753194,
87126858,
88507258,
89894410,
91288330,
92693002,
94108458,
95534730,
96971850,
98419850,
99878762,
101348618,
102829450,
104321290,
105824170,
107352234,
108905674,
110484682,
112089450,
113720170,
115377034,
117060234,
118769962,
120506410,
122269770,
124065890,
125895058,
127757562,
129653690,
131583730,
133547970,
135546698,
137580202,
139648770,
141752690,
143928178,
146176386,
148498466,
150895570,
153368850,
155919458,
158548546,
161257266,
164046770,
166918210,
169956978,
173167682,
176554930,
180123330,
205345890,
209100050,
212902550,
216753470,
220652890,
224600890,
228597550,
232642950,
236737170,
240880290,
245072390,
249313550,
253603850,
257943370,
262332190,
266770390,
271258050,
275795250,
280382070,
285018590,
289904810,
295042730,
300434350,
306081670,
311986690,
318151410,
324577830,
331267950,
338223770,
345447290,
354039310,
364049830,
375528850,
388526370,
403092390,
419276910,
437129930,
456701450,
476272970,
495844490,
515416010,
534987530,
554559050,
574130570,
593702090,
613273610,
632845130,
652416650,
671988170,
691559690,
711131210,
730702730,
750274250,
769845770,
789417290,
808988810,
828560330,
848131850,
867703370,
887274890,
906846410,
926417930,
945989450,
965560970,
985132490,
1004704010,
1024275530,
1043847050,
1063418570,
1082990090,
1102561610,
1122133130,
1141704650,
1161276170,
1180847690,
1200419210,
1222705731,
1244995262,
1267288477,
1289622601,
1311966887,
1334333102,
1356724650,
1379151914,
1401651370,
1424151231,
1448674779,
1473220997,
1497782544,
1522364697,
1546957043,
1571581919,
1596243411,
1620925875,
1645647464,
1670373305,
1754612555,
1864552110,
1985221455,
2000000000,
2039000000,
2078000000,
2117000000,
2156000000,
2195000000,
2234000000,
2273000000,
2312000000,
2351000000,
2390000000,
2429000000,
2468000000,
2507000000,
2546000000,
2585000000,
2624000000,
2663000000,
2702000000,
2741000000,
2780000000,
2819000000,
2858000000,
2897000000,
2936000000,
3000000000,
3043000000,
3086000000,
3129000000,
3172000000,
3215000000,
3250000000,
3301000000,
3344000000,
3387000000,
3430000000,
3473000000,
3516000000,
3559000000,
3602000000,
3645000000,
3688000000,
3731000000,
3774000000,
3817000000,
4000000000,
4100000000);

implementation
uses Util;
{ TCharacter }
function TCharacter.ClassLevel: TClassLevel;
begin
  if(Equip[0].Index = 1) OR (Equip[0].Index = 11) OR (Equip[0].Index = 21) OR (Equip[0].Index = 31) then
  begin
    Result := TClassLevel.Mortal;
    exit;
  end;
  Result := TClassLevel(Equip[0].Effects[1].Value);
end;

function TCharacter.GetBits(const Index: Integer): Byte;
begin
  Result := Util.GetBits(_MerchCity, Index);
end;

function TCharacter.HaveSkill(SkillId: Byte): Boolean;
var skillID2, aux: integer;
begin
  skillID2 := SkillId mod 24;
  aux := (Learn and (1 shl skillID2));
  Result := IfThen(aux <> 0);
end;

procedure TCharacter.SetBits(const Index: Integer; const Value: Byte);
begin
  SetByteBits(_MerchCity, Index, Value);
end;

{ TStatus }
function TStatus.GetMerchDir(const Index: Integer): Byte;
begin
  Result := GetBits(_MerchDir, Index);
end;
procedure TStatus.SetMerchDir(const Index: Integer; const Value: Byte);
begin
  SetByteBits(_MerchDir, Index, Value);
end;

function TStatus.GetMoveChaos(const Index: Integer): Byte;
begin
  Result := Util.GetBits(_MoveChaos, Index);
end;
procedure TStatus.SetMoveChaos(const Index: Integer; const Value: Byte);
begin
  SetByteBits(_MoveChaos, Index, Value);
end;

{ TAffectInfo }
function TAffectInfo.GetBits(const Index: Integer): Byte;
begin
  Result := Util.GetBits(_Info, Index);
end;

procedure TAffectInfo.SetBits(const Index: Integer; const Value: Byte);
begin
  SetByteBits(_Info, Index, Value);
end;

{ TParty }

function TParty.AddMember(memberClientId: WORD): Boolean;
begin

end;

end.


