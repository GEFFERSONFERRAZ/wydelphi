unit MiscData;

interface

uses SysUtils, Generics.Collections, Types;

type TEquipSlot =
(
  Face,
  Helmet,
  Chest,
  Pants,
  Gloves,
  Boots,
  LWeapon,
  RWeapon,
  Earring,
  SilverAmulet,
  Orb,
  Amulet,
  Guild,
  Fairy,
  Mount,
  Cape
);

type TDirection = (Forward, Backward, Rigth, Left);

type TItemAmount = Record
  ItemId: Integer;
  Slots: array[0..127] of BYTE;
  Amount: WORD;
  SlotsCount: BYTE;
End;

type TPosition = Record
  public
    X: SmallInt;
    Y: SmallInt;

    constructor Create(x, y: SmallInt);

    function Distance(const pos : TPosition) : WORD;
    function InRange(const pos : TPosition; range : WORD) : Boolean;
    procedure ForEach(range: Byte; proc: TProc<TPosition>);


    function magnitude: WORD;
    function normalized: TPosition;
    function IsValid: boolean;
    function PvPAreaType : Integer;

    class function Forward: TPosition; static;
    class function Rigth: TPosition; static;

    class function Lerp(const start, dest: TPosition; time: Single): TPosition; static;
    class function Qerp(const start, dest: TPosition; time: Single; inverse: Boolean = false): TPosition; static;
//    class function Cerp(const start, dest: TPosition; time: Single): TPosition; static;

    class operator Equal(pos1, pos2 : TPosition): Boolean;
    class operator NotEqual(pos1, pos2 : TPosition): Boolean;
    class operator Add(pos1, pos2 : TPosition) : TPosition;
    class operator Subtract(pos1, pos2 : TPosition) : TPosition;
    class operator Multiply(pos1 : TPosition; val: WORD): TPosition;
    class operator Multiply(pos1 : TPosition; val: Single): TPosition;
end;

type TRect = record
  BottomLeft, TopRigth: TPosition;
end;

type PQuest = ^TQuest;
TQuest = record
  Index: Integer;
  ItemId: Integer;
  ResetTime: Int8;
  QuestPosition: TPosition;
  QuestArea: TRect;

  Players: TList<WORD>;

  procedure ClearQuest;
end;

type TTeleport = Record
  Scr1, Scr2, Dest1, Dest2 : TPosition;
  Price, Time: Integer;
End;

type TItemEffect = Record
  Index, Value : BYTE;
End;

type PItem = ^TItem;
TItem = record
  Index: Word;
  Effects : array[0..2] of TItemEffect;
end;

type THeightMap = Record
  p: array[0..4095] of array[0..4095] of BYTE;
End;

type TTradeStore = Record
  Name: string[23];
  Item: array[0..11] of TItem;

  Slot: array[0..11] of BYTE;

  Gold: array[0..11] of integer;
  Unknown,index: smallint;
end;

type TPacketAffect = Record
  Time, Index : Byte;
End;

type TSkillData = Record
  Index : smallint;
  SkillPoint, TargetType, AffectTime : integer;
  ManaSpent : WORD;
  Delay: WORD;
  Range,
  InstanceType, InstanceValue, TickType, TickValue,
  AffectType, AffectValue, Act123, Act123_2,
  InstanceAttribute, TickAttribute, Aggressive,
  Maxtarget, PartyCheck, AffectResist, PassiveCheck, Name : String;
End;

type TAISegment = Record
  Position: TPosition;
  Say: string[80];
  WaitTime: WORD; // Em segundos
End;

type PMOBGener = ^TMOBGener;
TMOBGener = Record
  Id,
	Mode, // 0 - 3
	MinuteGenerate,// 4 - 7

	MaxNumMob, // 8 - 11

	MobCount, // 12 - 15

	MinGroup, // 16 - 19
	MaxGroup: integer;// 20 - 23

  SpawnSegment: TAISegment;     // S� vai ser usado pra definir o Spawn
  Segments : TList<TAISegment>;
//  Destination: TAISegment;

	FightAction, // 504 - 823
	FleeAction, // 824 - 1143
	DieAction: array[0..3] of string[79]; // 1144 - 1463

  Follower,
  Leader: string;

	Formation, // 1464 - 1467
	RouteType: integer; // 1468 - 1471

  procedure AddSegmentData(segmentId: Byte); overload;
  procedure AddSegmentData(segmentId : Byte; x, y: Integer); overload;
  procedure AddSegmentData(segmentId : Byte; waitTime: Integer); overload;
  procedure AddSegmentData(segmentId : Byte; say: string); overload;
end;

type TItemListOld = Record
  Index : smallint;
  Name: String[63];

  Mesh: Smallint;
  Submesh: Smallint;

  unknow: Smallint;

  Level: Smallint;
  STR: Smallint;
  INT: Smallint;
  DEX: Smallint;
  CON: Smallint;

  Effects: array[0..11] of TItemEffect;

  Price: Integer;
  Unique: Smallint;
  Pos: Word;

  Extreme: Smallint;
  Grade: Smallint;
end;

type TEffectsBinary = Record
    index: Smallint;
    value: Smallint;
end;

type TItemList = Record
    Name: array[0..63] of AnsiChar;

    Mesh: Smallint;
	  Submesh: Smallint;

	  unknow: Smallint;

    Level: Smallint;
    STR: Smallint;
    INT: Smallint;
    DEX: Smallint;
    CON: Smallint;

    Effects: array[0..11] of TEffectsBinary;

    Price: Integer;
    Unique: Smallint;
    Pos: Word;

    Extreme: Smallint;
    Grade: Smallint;
end;

implementation

uses Util;

{ TPosition }
constructor TPosition.Create(x, y: SmallInt);
begin
  self.X := x;
  self.Y := Y;
end;

function TPosition.Distance(const pos: TPosition): WORD;
var dif: TPosition;
begin
  dif := self - pos;

  Result := Round(Sqrt(dif.X * dif.X + dif.Y * dif.Y));
end;


function TPosition.InRange(const pos : TPosition; range: WORD): Boolean;
var dist : WORD;
begin
  dist := Distance(pos);
  Result := IfThen(dist <= range);
end;

procedure TPosition.ForEach(range: Byte; proc: TProc<TPosition>);
var x, y: WORD;
begin
  for x := self.X - range to self.X + range do
  begin
    for y := self.Y - range to self.Y + range do
    begin
      if (x > 4096) or (x <= 0) or (y > 4096) or (y <= 0) then
        continue;

      proc(TPosition.Create(x,y));
    end;
  end;
end;

class function TPosition.Forward: TPosition;
begin
  Result.X := 0;
  Result.Y := -1;
end;
class function TPosition.Rigth: TPosition;
begin
  Result.X := -1;
  Result.Y := 0;
end;

function TPosition.IsValid: boolean;
begin
  Result := true;
  if(self.X > 4096) or (self.Y > 4096) or (self.X <= 0) or (self.Y <= 0) then
    Result := false;
end;

class function TPosition.Lerp(const start, dest: TPosition; time: Single): TPosition;
begin
  Result := start + (dest - start) * time;
end;

class function TPosition.Qerp(const start, dest: TPosition; time: Single; inverse: Boolean = false): TPosition;
var quad: Single;
begin
  quad := IfThen(inverse, (2 - time), time);
  Result := start + (dest - start) * time * quad;
end;


function TPosition.magnitude: WORD;
begin
  Result := Round(Sqrt(self.X * self.X + self.Y * self.Y));
end;


function TPosition.normalized: TPosition;
var mag : WORD;
begin
  mag := self.magnitude;

  Result.X := self.X div mag;
  Result.Y := self.Y div mag;
end;


class operator TPosition.Equal(pos1, pos2: TPosition): Boolean;
begin
  Result := false;
  if (pos1.X = pos2.X) AND (pos1.Y = pos2.Y) then
    Result := true;
end;
class operator TPosition.NotEqual(pos1, pos2: TPosition): Boolean;
begin
  Result := not(pos1 = pos2);
end;

function TPosition.PvPAreaType: Integer;
begin
	////MENOS CP / COM FRAG
	if (X >= 3330) AND (Y >= 1026) AND (X <= 3600) AND (Y <= 1660) then Result := 2 //Area das Pistas de Runas
	else if (X >= 2176) AND (Y >= 1150) AND (X <= 2304) AND (Y <= 1534) then Result := 2 //Area Campo Azran Quest Imp
	else if (X >= 2446) AND (Y >= 1850) AND (X <= 2546) AND (Y <= 1920) then Result := 2 //Area Torre Erion 02
	else if (X >= 1678) AND (Y >= 1550) AND (X <= 1776) AND (Y <= 1906) then Result := 2 //Area de Reinos
	else if (X >= 1150) AND (Y >= 1676) AND (X <= 1678) AND (Y <= 1920) then Result := 2 //Area ca�a Noatun
	else if (X >= 3456) AND (Y >= 2688) AND (X <= 3966) AND (Y <= 3083) then Result := 2 //Area ca�a Gelo
	else if (X >= 3582) AND (Y >= 3456) AND (X <= 3968) AND (Y <= 3710) then Result := 2 //Area Lan House

	////SEM CP / SEM FRAG
	else if (X >= 2602) AND (Y >= 1702) AND (X <= 2652) AND (Y <= 1750) then Result := 1 //Area Coliseu Azran
	else if (X >= 2560) AND (Y >= 1682) AND (X <= 2584) AND (Y <= 1716) then Result := 1 //Area PVP Azran
	else if (X >= 2122) AND (Y >= 2140) AND (X <= 2148) AND (Y <= 2156) then Result := 1 //Area PVP Armia
	else if (X >= 136) AND (Y >= 4002) AND (X <= 200) AND (Y <= 4088) then Result := 1 //Area Duelo

	////SEM CP / COM FRAG
	else if (X >= 2174) AND (Y >= 3838) AND (X <= 2560) AND (Y <= 4096) then Result := 3 //Area Kefra
	else if (X >= 1076) AND (Y >= 1678) AND (X <= 1150) AND (Y <= 1778) then Result := 3 //Area Castelo Noatun
	else if (X >= 1038) AND (Y >= 1678) AND (X <= 1076) AND (Y <= 1702) then Result := 3 //Area Castelo Noatun Altar
	else if (X >= 2498) AND (Y >= 1868) AND (X <= 2516) AND (Y <= 1896) then Result := 3 //Area Torre Erion 01
	else if (X >= 130) AND (Y >= 140) AND (X <= 248) AND (Y <= 240) then Result := 3 //Area Guerra entre Guildas
	else Result := 0;
end;

class operator TPosition.Add(pos1, pos2: TPosition): TPosition;
begin
  Result.X := pos1.X + pos2.X;
  Result.Y := pos1.Y + pos2.Y;
end;
class operator TPosition.Subtract(pos1, pos2: TPosition): TPosition;
begin
  Result.X := pos1.X - pos2.X;
  Result.Y := pos1.Y - pos2.Y;
end;
class operator TPosition.Multiply(pos1 : TPosition; val: WORD): TPosition;
begin
  Result.X := pos1.X * val;
  Result.Y := pos1.Y * val;
end;
class operator TPosition.Multiply(pos1: TPosition; val: Single): TPosition;
begin
  Result.X := Round(pos1.X * val);
  Result.Y := Round(pos1.Y * val);
end;


{ TQuest }
procedure TQuest.ClearQuest;
var index: Integer;
begin
//  TFunctions.ClearArea(QuestArea.BottomLeft, QuestArea.TopRigth);
  self.Players.Clear;
end;

{ TMOBGener }

procedure TMOBGener.AddSegmentData(segmentId: Byte; x, y: Integer);
var segment: TAISegment;
begin
  AddSegmentData(segmentId);
  Dec(segmentId);

  segment := Segments[segmentId];
  if x <> -1 then
    segment.Position.X := x;
  if y <> -1 then
    segment.Position.Y := y;

  Segments[segmentId] := segment;
end;

procedure TMOBGener.AddSegmentData(segmentId: Byte; waitTime: Integer);
var segment: TAISegment;
begin
  AddSegmentData(segmentId);
  Dec(segmentId);

  segment := Segments[segmentId];
  segment.WaitTime := waitTime;
  Segments[segmentId] := segment;
end;

procedure TMOBGener.AddSegmentData(segmentId: Byte; say: string);
var segment: TAISegment;
begin
  AddSegmentData(segmentId);
  Dec(segmentId);

  segment := Segments[segmentId];
  segment.Say := say;
  Segments[segmentId] := segment;
end;

procedure TMOBGener.AddSegmentData(segmentId: Byte);
var segment: TAISegment;
begin
  if Segments = NIL then
    Segments := TList<TAISegment>.Create;

  while segmentId > Segments.Count do
  begin
    Segments.Add(segment);
  end;
end;

end.
