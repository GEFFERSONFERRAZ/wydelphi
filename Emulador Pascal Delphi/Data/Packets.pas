unit Packets;

interface

Uses PlayerData, MiscData, Util;


type TPacketHeader = Record
  Size: Smallint;
  Key: Byte;
  ChkSum: Byte;
  Code: Smallint;
  Index: Smallint;
  Time: LongWord;
end;

type TSignalData = Record
  Header : TPacketHeader;
  Data : integer;
end;

type TNpcPacket = Record
  Header : TPacketHeader;
  NpcIndex: WORD;
  //Unknow: array[0..5] of BYTE;
  Unknow: WORD;
  Confirm: WORD;
end;

type TMestreGriffo = Record
	Header   : TPacketHeader;
	WarpID   : integer;
	WarpType : integer;
end;

// Request Emotion
type TRequestEmotion = Record
  Header  : TPacketHeader;
  effType : smallint;
  effValue: smallint;
  Unknown1: integer;
end;

type TDeleteItem = Record
  Header: TPacketHeader;
  slot,itemid: integer;
end;

type TGroupItem = Record
  Header: TPacketHeader;
  slot,itemid,quant: integer;
end;

type TRequestLoginPacket = record
  Header: TPacketHeader;
  UserName: array[0..15] of AnsiChar;
  PassWord: array[0..11] of AnsiChar;

  Version: integer;
  unk1: integer;

  Keys: String[15];
  Address: String[15];
end;

type TRefreshMoneyPacket = record
  Header: TPacketHeader;
  Gold: integer;
end;

type TRefreshEtcPacket = record
  Header: TPacketHeader;
  Hold: integer;
  Exp: integer;
  Learn: integer;
  StatusPoint: smallint;
  MasterPoint: smallint;
  SkillsPoint: smallint;
  MagicIncrement: smallint;
  Gold: integer;
end;

type TClientMessagePacket = record
  Header  : TPacketHeader;
  Message : string[95];
end;

type TRefreshInventoryPacket = record
  Header : TPacketHeader;
  Inventory : array[0..63] of TItem;
  Gold : integer;
end;

type TSendCurrentHPMPPacket = record
	Header: TPacketHeader;
	CurHP : WORD;
	CurMP : WORD;
  MaxHP : WORD;
	MaxMP : WORD;
end;


type TSendScorePacket = record
  Header: TPacketHeader; //12
  Score : TStatus;       //40
  Critical: BYTE;
  SaveMana: BYTE;

  Affects: array[0..15] of TPacketAffect;

	GuildMemberType, GuildIndex : BYTE;

	RegenHP, RegenMP : BYTE;

  Resist : array[0..3] of shortint;

	CurHP, CurMP : WORD;

	MagicIncrement : BYTE;
	Unknown: array[0..4] of BYTE;
end;

// Request Command
type TCommandPacket = record
  Header: TPacketHeader;
  //Command: String[15];
  //Value: String[99];
  Command: array[0..15] of AnsiChar;
  Value: array[0..99] of AnsiChar;
end;

type TChatPacket = record
  Header: TPacketHeader;
  Chat: string[95];
end;


type TMessageAction = Record
  PosX, PosY: WORD;
  Speed : Integer;
  Effect: SmallInt;		// 0:앉기  1:서기  2:걷기  3:뛰기  4:날기  5:텔레포트,	6:밀리기(knock-back), 7:미끄러지기(이동애니없음)  8:도발, 9:인사, 10:돌격
  Direction: SmallInt;	//
  TargetX,TargetY: WORD;
end;

// Request Add Points
type TRequestAddPoints = Record
  Header: TPacketHeader;
  Mode,Info: smallint;
  Unk: integer;
end;

type TCharListCharactersData = Record
  Position: array[0..3] of TPosition;
  Name: array[0..3] of Array[0..15]of AnsiChar;

  Status: array[0..3] of tStatus;
  Equip: array[0..3] of array[0..15] of tItem;

  GuildIndex: array[0..3] of Word;

  Gold: array[0..3] of Integer;
  Exp: array[0..3] of Integer;
end;

type TSendToCharListPacket = Record
  Header : TPacketHeader;
  CharactersData : TCharListCharactersData;
  Storage : array[0..127] of TItem;

  Gold : Integer;
  Name : String[15];
  Keys : String[15];

  unk1 : Integer;
  unk2 : Integer;
end;

type TNumericTokenPacket = Record
	Header: TPacketHeader;
	num: array[0..5] of AnsiChar;
  unk: array[0..9] of AnsiChar;
	RequestChange: integer;
end;

type TCreateCharacterRequestPacket = Record
  Header: TPacketHeader;
  SlotIndex: integer;
  Name: array[0..15] of AnsiChar;
  ClassIndex: integer;
end;

type TUpdateCharacterListPacket = Record
  Header: TPacketHeader;
  CharactersData : TCharListCharactersData;
end;

type TDeleteCharacterRequestPacket = Record
  Header: TPacketHeader;
  SlotIndex: Integer;
  Name: array[0..15] of AnsiChar;
  Password: array[0..11] of AnsiChar;
end;

type TSelectCharacterRequestPacket = Record
  Header : TPacketHeader;
  CharacterId : Byte;
End;

type TSendToWorldPacket = Record
  Header: TPacketHeader;
  Point: TPosition;
  Character: TCharacter;
  Zero: array[0..(1244 - (sizeof(TCharacter) + sizeof(TPosition) + 12))] of byte;
end;

type TAffectInPacket = Record
  Time: BYTE;
  Index: BYTE;
end;

type TSendNPCSellItensPacket = Record
  Header: TPacketHeader;
  Merch: integer;

  Itens: array[0..26] of TItem;

  Imposto: integer;
End;


type TSellItemsToNpcPacket = Record
  Header: TPacketHeader;
  mobID,invType: smallint;
  invSlot: integer;
end;

type TBuyNpcItensPacket = Record
  Header: TPacketHeader;
  mobID,sellSlot: smallint;
  invSlot,Unknown1: smallint;
  Unknown2: integer;
end;

type p2E4 = Record
  Header: TPacketHeader;
  slot, itemid: integer;
end;

//Send Party Request
type TPartyRequestPacket = Record
  Header: TPacketHeader;
  LeaderId: WORD;
  Level: WORD;
  MaxHp, CurHp: WORD;
  SenderId: WORD;
  Nick: array[0..15] of AnsiChar;//string[15];
  unk2: BYTE;
  TargetId: WORD;
end;

type TAcceptPartyPacket = Record
  Header: TPacketHeader;
  LeaderId: WORD;
  Nick: array[0..15] of AnsiChar;
end;

type TExitPartyPacket = Record
  Header: TPacketHeader;
  ExitId: WORD;
  unk: Word;//$CCCC
end;

type TSendPartyMember = Record
  Header: TPacketHeader;
  LiderID: WORD;
  Level: WORD;
  MaxHp, CurHp: WORD;
  ClientId: WORD;
  Nick: string[15];
  unk2: WORD;
end;

type TUseItemPacket = Record
  Header: TPacketHeader;
  SrcType, SrcSlot: integer;
  DstType, DstSlot: integer;

  Position : TPosition;
  unk: integer;
end;

// Request Drop Item
type TReqDropItem = Record
  Header: TPacketHeader;
  invType,
  InvSlot,
  Unknown1 : Integer;
  Position : TPosition;
  Unknown2 : Integer;
end;

// Request Pick Item
type TReqPickItem = Record
  Header : TPacketHeader;
  invType,
  InvSlot : Integer;
  initId : WORD;
  Position : TPosition;
  Unknown1 : WORD;
end;

type TDropDelete = Record
  Header : TPacketHeader;
  initId : WORD;
  Unknown1 : WORD;
End;

// MOB DEAD
type
p_p338 = ^TSendMobDeadPacket;

TSendMobDeadPacket = Record
	Header: TPacketHeader;
	Hold: integer;
  killed, killer: smallint;
	Exp: integer;
end;

type mob_kill = Record
  Hold, Exp: Integer;
  EnemyList, EnemyIndex: pInteger;
  Dead: boolean;
  inBattle: pBoolean;
end;


type TSendCreateMobPacket = Record
  Header: TPacketHeader;

  Position : TPosition;

  ClientId: WORD;
  Name: array[0..11] of AnsiChar;

  ChaosPoint: BYTE;
  CurrentKill: BYTE;
  TotalKill: WORD;

  ItemEff: array[0..15] of WORD;

  Affect: array[0..15] of TPacketAffect;

  GuildIndex: WORD;

  Status: TStatus;

  SpawnType: BYTE;
  MemberType: BYTE;

  AnctCode: array[0..15] of BYTE;

  //Tab: string[25];
  Tab : array[0..25] of AnsiChar;

  unk: array[0..3] of BYTE;
end;

type TSendCreateMobTradePacket = Record
  Header: TPacketHeader;

  Position : TPosition;

  Index: WORD;
  Name: array[0..11] of AnsiChar;

  ChaosPoint: BYTE;
  CurrentKill: BYTE;
  TotalKill: WORD;

  ItemEff: array[0..15] of WORD;

  Affect: array[0..15] of TPacketAffect;

  GuildIndex: WORD;

  Status: TStatus;

  spawnType: BYTE;
  MemberType: BYTE;

  unk: array[0..13] of BYTE;
  Clientid,unk2,y2,x2,clock : integer;

  unk3: array[0..7] of BYTE;

  StoreName: string[23];//Array[0..23]of AnsiChar;
  unk4: array[0..3] of BYTE;
  //tab: string[200];
end;


type TTradePacket = Record
	Header: TPacketHeader;

	TradeItem: array[0..14] of TItem;
	TradeItemSlot: array[0..14] of shortint;

	Unknow: BYTE;

	Gold: integer;
	Confirm: boolean;
	OtherClientId: WORD;
end;

// Request Open Trade - 39A

//Open Trade
type TOpenTrade = Record
  Header: TPacketHeader;
  OtherClientId: integer;
end;

// Request Buy Item Trade
type TBuyStoreItemPacket = Record
  Header: TPacketHeader;
  Slot: integer;
  SellerId: integer;
  Gold: integer;
  Unknown: integer;
  Item: TItem;
end;

type TSendItemBoughtPacket = Record
  Header: TPacketHeader;
  SellerId: Integer;
  Slot: Integer;
end;

// Request Create Item
type TSendCreateItemPacket = record
  Header: TPacketHeader;
  invType: smallint;
  invSlot: smallint;
  itemData: TItem;
end;

// Send Delete Item
type TSendDeleteItemPacket = record
  Header: TPacketHeader;
  invType: integer;
  invSlot: integer;
  Unknown: integer;
  Pos : TPosition;
end;

type TSendCreateDropItem = Record
  Header: TPacketHeader;
  Pos : TPosition;
  initId : WORD;
  item: TItem;
  rotation,
  status : BYTE;
  Unknown: integer;
End;
// Request Refresh Inventory



// Request Refresh Etc


// Request Move Item
type TMoveItemPacket = record
  Header: TPacketHeader;
  DestType: BYTE;
  destSlot: BYTE;
  SrcType: BYTE;
  srcSlot: BYTE;
  Unknown: integer;   //provavel gold banco
end;

// Request Refresh Itens
type TRefreshEquips = record
  private
    function GetBits(Index : BYTE; const aIndex: Integer): WORD;
    procedure SetBits(Index : BYTE; const aIndex: Integer; const aValue: WORD);

  public
    Header: TPacketHeader;

    itemIDEF: array[0..15] of WORD;
    {
        unsigned short ItemID : 12;
        unsigned short Sanc : 4;
    } //ItemEff[16];

    pAnctCode: array[0..15] of shortint;

    property Sanc[Index : BYTE] : WORD index $0C04 read GetBits write SetBits;
end;

type TRequestOpenPlayerStorePacket = Record
  Header: TPacketHeader;
  Trade: TTradeStore;
end;


type TTarget = Record
  Index, Damage: WORD;
end;

//Attack Sigle Mob
type TProcessAttackOneMob = Record
	Header: TPacketHeader;
	AttackerID, AttackCount: smallint; // Id de quem Realiza o ataque
	AttackerPos: TPosition; // Posicao X e Y de quem Ataca
	TargetPos: TPosition; // Posicao X e Y de quem Sofre o Ataque
	SkillIndex: smallint; // Id da skill usada
	CurrentMp: smallint; // Mp atual de quem Ataca
	Motion: shortint;   // (*)
	SkillParm: shortint; // (*)
	FlagLocal: shortint; // (*)
	DoubleCritical: shortint; // 0 para critico Simples, 1 para critico Duplo
	Hold ,CurrentExp: integer;
	ReqMp: smallint; // Mp necessario para usar a Skill
	Rsv: smallint;  // (*)
  Target: TTarget;
end;


//Ataque em area
type TProcessAoEAttack = Record
	Header: TPacketHeader;
	AttackerID,AttackCount: smallint; // Id de quem Realiza o ataque
	AttackerPos: TPosition; // Posicao X e Y de quem Ataca
	TargetPos: TPosition; // Posicao X e Y de quem Sofre o Ataque
	SkillIndex: smallint; // Id da skill usada
	CurrentMp: smallint; // Mp atual de quem Ataca
	Motion: shortint;
	SkillParm: shortint;
	FlagLocal: shortint;
	DoubleCritical: shortint; // 0 para critico Simples, 1 para critico Duplo
	Hold ,CurrentExp: integer;
	ReqMp: smallint; // Mp necessario para usar a Skill
	Rsv: smallint;
	Targets: array[0..12] of TTarget;
end;


//atack reto
type
p_p39E = ^p39E;
p39E = Record
	Header: TPacketHeader;
	AttackerID,AttackCount: smallint; // Id de quem Realiza o ataque
	AttackerPos: TPosition; // Posicao X e Y de quem Ataca
	TargetPos: TPosition; // Posicao X e Y de quem Sofre o Ataque
	SkillIndex: smallint; // Id da skill usada
	CurrentMp: smallint; // Mp atual de quem Ataca
	Motion: shortint;
	SkillParm: shortint;
	FlagLocal: shortint;
	DoubleCritical: shortint; // 0 para critico Simples, 1 para critico Duplo
	Hold ,CurrentExp: integer;
	ReqMp: smallint; // Mp necessario para usar a Skill
	Rsv: smallint;

  Target: array[0..1] of TTarget;
end;

// Request Emotion
type TSendEmotionPacket = Record
  Header : TPacketHeader;
  effType, effValue: smallint;
  Unknown1: integer;
end;

type TSendWeatherPacket = Record
  Header: TPacketHeader;
  WeatherId: Integer;
End;

type TSkillBarChange = Record
  Header  : TPacketHeader;
  SkillBar: array[0..19] of BYTE;
End;

type TCompoundersPacket = record
	  Header : TPacketHeader;
    Item: array[0..7] of TItem;
	  Slot: array[0..7] of BYTE;
end;

//SendAffect
type TSendAffectsPacket = record
	Header: TPacketHeader;
	Affects: array[0..15] of TAffect;
end;

// Effect Defines
//
Const

	EF_NONE		    =	0;
// Status
 EF_LEVEL				 =	1;
 EF_DAMAGE			 =  2;
 EF_AC					 =	3;
 EF_HP					 =	4;
 EF_MP					 =	5;
 EF_EXP					 =	6;
 EF_STR					 =	7;
 EF_INT					 =	8;
 EF_DEX					 =	9;
 EF_CON					 =	10;
 EF_SPECIAL1		 =	11;
 EF_SPECIAL2		 =  12;
 EF_SPECIAL3		 =  13;
 EF_SPECIAL4		 =	14;
 EF_SCORE14			 =	15;
 EF_SCORE15			 =	16;

// Requeriment
 EF_POS					 =	17;
 EF_CLASS				 =	18;
 EF_R1SIDC			 =	19;
 EF_R2SIDC			 =	20;
 EF_WTYPE				 =	21;
 EF_REQ_STR			 =	22;
 EF_REQ_INT			 =	23;
 EF_REQ_DEX			 =	24;
 EF_REQ_CON			 =	25;

// Bonus
 EF_ATTSPEED		 =			26;
 EF_RANGE				 =	27;
 EF_PRICE				 =	28;
 EF_RUNSPEED		 =			29;
 EF_SPELL				 =	30;
 EF_DURATION		 =			31;
 EF_PARM2				 =	32   ;
 EF_GRID				 =		33  ;
 EF_GROUND			 =		34  ;
 EF_CLAN				 =		35;
 EF_HWORDCOIN		 =		36;
 EF_LWORDCOIN		 =		37;
 EF_VOLATILE		 =			38;
 EF_KEYID				 =	39    ;
 EF_PARRY				 =	40     ;
 EF_HITRATE			 =		41    ;
 EF_CRITICAL		 =			42   ;
 EF_SANC				 =		43      ;
 EF_SAVEMANA		 =			44     ;
 EF_HPADD				 =	45        ;
 EF_MPADD				 =	46       ;
 EF_REGENHP			 =		47    ;
 EF_REGENMP			 =		48    ;
 EF_RESIST1			 =		49    ;
 EF_RESIST2			 =		50    ;
 EF_RESIST3			 =		51    ;
 EF_RESIST4			 =		52    ;
 EF_ACADD				 =	53      ;
 EF_RESISTALL		 =		54    ;
 EF_BONUS				 =	55      ;
 EF_HWORDGUILD	 =			56  ;
 EF_LWORDGUILD	 =			57  ;
 EF_QUEST				 =	58      ;
 EF_UNIQUE			 =		59    ;
 EF_MAGIC				 =	60      ;
 EF_AMOUNT			 =		61    ;
 EF_HWORDINDEX	 =			62  ;
 EF_LWORDINDEX	 =			63  ;
 EF_INIT1				 =	64      ;
 EF_INIT2				 =	65      ;
 EF_INIT3				 =	66      ;
 EF_DAMAGEADD		 =		67    ;
 EF_MAGICADD		 =			68  ;
 EF_HPADD2			 =		69    ;
 EF_MPADD2			 =		70    ;
 EF_CRITICAL2		 =		71    ;
 EF_ACADD2			 =		72    ;
 EF_DAMAGE2			 =		73    ;
 EF_SPECIALALL	 =			74  ;

// Mount
	EF_CURKILL		=			75   ;
 EF_LTOTKILL		=			76   ;
 EF_HTOTKILL		=			77   ;
 EF_INCUBATE		=			78   ;
 EF_MOUNTLIFE		=		79     ;
 EF_MOUNTHP			=		80     ;
 EF_MOUNTSANC	 =			81     ;
 EF_MOUNTFEED	 =			82     ;
 EF_MOUNTKILL	 =			83     ;
 EF_INCUDELAY	 =			84     ;
 EF_SUBGUILD	 =				85   ;

// Set Option
 EF_GRADE0			=		100     ;
 EF_GRADE1		 =			101     ;
 EF_GRADE2		=			102     ;
 EF_GRADE3	 =				103     ;
 EF_GRADE4	=				104     ;
 EF_GRADE5 =					105     ;




type TMovementPacket = Record
  Header : TPacketHeader;
  Source : TPosition;
  Speed : integer;
  MoveType : integer;

  Destination : TPosition;
  Command : array [0..23] of AnsiChar;
end; // p366 and p367

type TMoveCommandPacket = Record // 52 -> 17
  Header: TPacketHeader;
  Destination: TPosition;
  Speed : Byte;
end;


implementation

uses System.Threading;

function TRefreshEquips.GetBits(Index : BYTE; const aIndex: Integer): WORD;
begin
  Result := GetBits(itemIDEF[Index], aIndex);
end;

procedure TRefreshEquips.SetBits(Index : BYTE; const aIndex: Integer; const aValue: WORD);
begin
  SetWordBits(itemIDEF[Index], aIndex, aValue);
end;

end.
